<?php
if (!defined('ABSPATH')) exit;

class WpsolCdnIntegration
{
    /**
     * WpsolCdnIntegration constructor.
     */
    public function __construct()
    {
        add_action('load-wp-speed-of-light_page_wpsol_cdn_integration', array($this, 'saveSettings'));
        add_action('template_redirect', array($this, 'handleRewriteCdn'));
    }

    /**
     *  Save settings
     */
    public function saveSettings()
    {
        if (current_user_can('manage_options')) {
            if (!empty($_REQUEST['action']) && 'wpsol_save_cdn' === $_REQUEST['action']) {
                check_admin_referer('wpsol_cdn_integration', '_wpsol_cdn_nonce');

                $cdn_content = array();
                $cdn_exclude_content = array();
                if (!empty($_REQUEST['cdn-content'])) {
                    $cdn_content = explode(',', $_REQUEST['cdn-content']);
                }

                if (!empty($_REQUEST['cdn-exclude-content'])) {
                    $cdn_exclude_content = explode(',', $_REQUEST['cdn-exclude-content']);
                }

                $cdn_settings = array(
                    'cdn_active' => (isset($_REQUEST['cdn-active'])) ? 1 : 0,
                    'cdn_url' => $_REQUEST['cdn-url'],
                    'cdn_content' => $cdn_content,
                    'cdn_exclude_content' => $cdn_exclude_content,
                    'cdn_relative_path' => (isset($_REQUEST['cdn-relative-path'])) ? 1 : 0
                );

                if (class_exists('WpsolAddonCDNIntegration')) {
                    $cdn_settings = apply_filters('wpsol_addon_save_cdn_integration', $cdn_settings, $_REQUEST);
                }

                update_option('wpsol_cdn_integration', $cdn_settings);

                if (!empty($_REQUEST['_wp_http_referer'])) {
                    wp_redirect($_REQUEST['_wp_http_referer'] . '&save-settings=success');
                    exit;
                }
            }
        }
    }

    /**
     * Execute rewrite cdn
     */
    public function handleRewriteCdn()
    {
        $cdn_integration = get_option('wpsol_cdn_integration');

        if (empty($cdn_integration)) {
            return;
        }

        if ($cdn_integration['cdn_url'] == '') {
            return;
        }

        if (get_option('home') == $cdn_integration['cdn_url']) {
            return;
        }

        $rewrite = new WpsolCDNRewrite($cdn_integration);

        //rewrite CDN Url to html raw
        add_filter('wpsol_cdn_content_return', array(&$rewrite, 'rewrite'));
    }
}
