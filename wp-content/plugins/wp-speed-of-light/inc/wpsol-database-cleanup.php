<?php
if (!defined('ABSPATH')) exit;

class WpsolDatabaseCleanup
{
    /**
     * Use query to clean system
     * @param $type
     * @return string
     */
    public static function cleanSystem($type)
    {
        $clean = "";
        check_admin_referer('wpsol_database_clean', '_wpsol_nonce');

        self::cleanupDb($type);
        $message = "Database cleanup successful";

        return $message;
    }

    /**
     * Exclude clean element
     * @param $type
     */
    public static function cleanupDb($type)
    {
        global $wpdb;

        switch ($type) {
            case "revisions":
                $clean = "DELETE FROM `$wpdb->posts` WHERE post_type = 'revision';";
                $revisions = $wpdb->query($clean);
                break;
            case "drafted":
                $clean = "DELETE FROM `$wpdb->posts` WHERE post_status = 'auto-draft';";
                $autodraft = $wpdb->query($clean);
                break;
            case "trash":
                $clean = "DELETE FROM `$wpdb->posts` WHERE post_status = 'trash';";
                $posttrash = $wpdb->query($clean);
                break;
            case "comments":
                $clean = "DELETE FROM `$wpdb->comments` WHERE comment_approved = 'spam' OR comment_approved = 'trash';";
                $comments = $wpdb->query($clean);
                break;
            case "trackbacks":
                $clean = "DELETE FROM `$wpdb->comments` WHERE comment_type = 'trackback' OR comment_type = 'pingback';";
                $comments = $wpdb->query($clean);
                break;
            case "transient":
                $clean = "DELETE FROM `$wpdb->options` WHERE option_name LIKE '%\_transient\_%';";
                $comments = $wpdb->query($clean);
                break;
        }

        do_action('wpsol_addon_optimize_and_clean_duplicate_table', $type);
    }


    /**
     * Count element which to cleanup
     * @param $type
     * @return false|int
     */
    public function getElementToClean($type)
    {
        global $wpdb;
        $return = 0;
        switch ($type) {
            case "revisions":
                $element = "SELECT * FROM `$wpdb->posts` WHERE post_type = 'revision';";
                $return = $wpdb->query($element);
                break;
            case "drafted":
                $element = "SELECT * FROM `$wpdb->posts` WHERE post_status = 'auto-draft';";
                $return = $wpdb->query($element);
                break;
            case "trash":
                $element = "SELECT * FROM `$wpdb->posts` WHERE post_status = 'trash';";
                $return = $wpdb->query($element);
                break;
            case "comments":
                $element = "SELECT * FROM `$wpdb->comments`";
                $element .= " WHERE comment_approved ='spam' OR comment_approved = 'trash';";
                $return = $wpdb->query($element);
                break;
            case "trackbacks":
                $element = "SELECT * FROM `$wpdb->comments` ";
                $element .= " WHERE comment_type = 'trackback' OR comment_type = 'pingback';";
                $return = $wpdb->query($element);
                break;
            case "transient":
                $element = "SELECT * FROM `$wpdb->options` WHERE option_name LIKE '%\_transient\_%';";
                $return = $wpdb->query($element);
                break;
        }
        $return = apply_filters('wpsol_addon_count_number_db', $return, $type);
        return $return;
    }
}
