<?php
if (!defined('ABSPATH')) exit;

class WpsolSpeedOptimization
{

    public $cache;
    public $minification;

    /**
     * WpsolSpeedOptimization constructor.
     */
    public function __construct()
    {
        $this->cache = array('speed_optimization' => array());
        $this->minification = array('advanced_features' => array());
    }

    public function setAction()
    {

        add_action('load-wp-speed-of-light_page_wpsol_speed_optimization', array($this, 'saveSettings'));

        if (class_exists('WpsolAddonSpeedOptimization')) {
            $check_on_save = apply_filters('wpsol_addon_check_cleanup_on_save', false);

            if ($check_on_save) {
                add_action('pre_post_update', array($this, 'purgePostOnUpdate'), 10, 1);
                add_action('save_post', array($this, 'purgePostOnUpdate'), 10, 1);
            }
        } else {
            add_action('pre_post_update', array($this, 'purgePostOnUpdate'), 10, 1);
            add_action('save_post', array($this, 'purgePostOnUpdate'), 10, 1);
        }

        add_action('wp_trash_post', array($this, 'purgePostOnUpdate'), 10, 1);
        add_action('comment_post', array($this, 'purgePostOnNewComment'), 10, 3);
        add_action('wp_set_comment_status', array($this, 'purgePostOnCommentStatusChange'), 10, 2);
        add_action('set_comment_cookies', array($this, 'setCommentCookieExceptions'), 10, 2);

        // Remove query strings
        add_filter('wpsol_query_strings_return', array($this, 'removeQueryStrings'));
    }

    /**
     * When user posts a comment, set a cookie so we don't show them page cache
     *
     * @param  WP_Comment $comment
     * @param  WP_User $user
     * @since  1.3
     */
    public function setCommentCookieExceptions($comment, $user)
    {
        $config = get_option('wpsol_optimization_settings');
        // File based caching only
        if (!empty($config) && !empty($config['speed_optimization']['act_cache'])) {
            $post_id = $comment->comment_post_ID;

            setcookie(
                'wpsol_commented_posts[' . $post_id . ']',
                parse_url(get_permalink($post_id), PHP_URL_PATH),
                (time() + HOUR_IN_SECONDS * 24 * 30)
            );
        }
    }

    /**
     * action when save settings optimization
     */
    public function saveSettings()
    {
        $opts = get_option('wpsol_optimization_settings');
        //save setting speed optimization
        if (!empty($_REQUEST['action']) && 'wpsol_save_settings' === $_REQUEST['action']) {
            check_admin_referer('wpsol_speed_optimization', '_wpsol_nonce');
            $this->saveSettingsSpeedOptimization($opts);
            WP_Filesystem();

            WpsolCache::factory()->write();
            //write config for cache
            WpsolCache::factory()->writeConfigCache();

            if (!empty($this->cache) && !empty($this->cache['speed_optimization']['act_cache'])) {
                WpsolCache::factory()->toggleCaching(true);
            } else {
                WpsolCache::factory()->toggleCaching(false);
            }
            // Reschedule cron events
            WpsolCleanCacheTime::factory()->unscheduleEvents();
            WpsolCleanCacheTime::factory()->scheduleEvents();
            //add expire header
            if (!empty($this->cache['speed_optimization']['add_expires'])) {
                $this->addExpiresHeader(true);
            } else {
                $this->addExpiresHeader(false);
            }

            //clear cache after save settings
            WpsolCache::wpsolCacheFlush();
            WpsolMinificationCache::clearMinification();
        }
        //save settings on minify page
        if (!empty($_REQUEST['action']) && 'wpsol_save_minification' === $_REQUEST['action']) {
            check_admin_referer('wpsol_minify', '_wpsol_nonce');
            $this->saveSettingsAdvancedFeatures($opts);
            //clear minification
            WpsolCache::wpsolCacheFlush();
            WpsolMinificationCache::clearMinification();
        }

        //save settings on advanced optimization page
        if (!empty($_REQUEST['action']) && 'wpsol_save_advanced_optimization' === $_REQUEST['action']) {
            check_admin_referer('wpsol_advanced_optimization', '_wpsol_nonce');

            if (class_exists('WpsolAddonSpeedOptimization')) {
                do_action('wpsol_addon_storage_advanced_optimization');
            }
            //clear minification
            WpsolCache::wpsolCacheFlush();
        }


        //return current page
        if (!empty($_REQUEST['_wp_http_referer'])) {
            wp_redirect($_REQUEST['_wp_http_referer'] . '&save-settings=success');
            exit;
        }
    }

    /**
     * Automatically purge all file based page cache on post changes
     * @param $post_id
     */
    public function purgePostOnUpdate($post_id)
    {
        $post_type = get_post_type($post_id);
        if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || 'revision' === $post_type) {
            return;
        } elseif (!current_user_can('edit_post', $post_id) && (!defined('DOING_CRON') || !DOING_CRON)) {
            return;
        }

        $config = get_option('wpsol_optimization_settings');

        // File based caching only
        if (!empty($config) && !empty($config['speed_optimization']['act_cache'])) {
            WpsolCache::wpsolCacheFlush();
        }
    }

    /**
     * Purge cache con new comment
     * @param $comment_ID
     * @param $approved
     * @param $commentdata
     */
    public function purgePostOnNewComment($comment_ID, $approved, $commentdata)
    {
        if (empty($approved)) {
            return;
        }
        $config = get_option('wpsol_optimization_settings');
        // File based caching only
        if (!empty($config) && !empty($config['speed_optimization']['act_cache'])) {
            $post_id = $commentdata['comment_post_ID'];

            global $wp_filesystem;

            if (empty($wp_filesystem)) {
                require_once(ABSPATH . '/wp-admin/includes/file.php');
                WP_Filesystem();
            }

            $url_path = get_permalink($post_id);
            if ($wp_filesystem->exists(untrailingslashit(WP_CONTENT_DIR) . '/cache/wpsol-cache/' . md5($url_path))) {
                $wp_filesystem->rmdir(untrailingslashit(WP_CONTENT_DIR) . '/cache/wpsol-cache/' . md5($url_path), true);
            }
        }
    }

    /**
     * if a comments status changes, purge it's parent posts cache
     * @param $comment_ID
     * @param $comment_status
     */
    public function purgePostOnCommentStatusChange($comment_ID, $comment_status)
    {
        $config = get_option('wpsol_optimization_settings');

        // File based caching only
        if (!empty($config) && !empty($config['speed_optimization']['act_cache'])) {
            $comment = get_comment($comment_ID);
            $post_id = $comment->comment_post_ID;

            global $wp_filesystem;

            WP_Filesystem();

            $url_path = get_permalink($post_id);

            if ($wp_filesystem->exists(untrailingslashit(WP_CONTENT_DIR) . '/cache/wpsol-cache/' . md5($url_path))) {
                $wp_filesystem->rmdir(untrailingslashit(WP_CONTENT_DIR) . '/cache/wpsol-cache/' . md5($url_path), true);
            }
        }
    }
    /**
     * Save settings physical
     * @param $opts
     */
    public function saveSettingsSpeedOptimization($opts)
    {
        if (!empty($opts)) {
            if (isset($_REQUEST['active-cache'])) {
                $this->cache['speed_optimization']['act_cache'] = 1;
            } else {
                $this->cache['speed_optimization']['act_cache'] = 0;
            }
            if (isset($_REQUEST['cache-desktop'])) {
                $this->cache['speed_optimization']['devices']['cache_desktop'] = (int)$_REQUEST['cache-desktop'];
            }
            if (isset($_REQUEST['cache-tablet'])) {
                $this->cache['speed_optimization']['devices']['cache_tablet'] = (int)$_REQUEST['cache-tablet'];
            }
            if (isset($_REQUEST['cache-mobile'])) {
                $this->cache['speed_optimization']['devices']['cache_mobile'] = (int)$_REQUEST['cache-mobile'];
            }
            if (isset($_REQUEST['query-strings'])) {
                $this->cache['speed_optimization']['query_strings'] = 1;
            } else {
                $this->cache['speed_optimization']['query_strings'] = 0;
            }
            if (isset($_REQUEST['add-expires'])) {
                $this->cache['speed_optimization']['add_expires'] = 1;
            } else {
                $this->cache['speed_optimization']['add_expires'] = 0;
            }

            if (isset($_REQUEST['cache_external_script'])) {
                $this->cache['speed_optimization']['cache_external_script'] = 1;
            } else {
                $this->cache['speed_optimization']['cache_external_script'] = 0;
            }

            if (isset($_REQUEST['remove_rest_api'])) {
                $this->cache['speed_optimization']['remove_rest_api'] = 1;
            } else {
                $this->cache['speed_optimization']['remove_rest_api'] = 0;
            }

            if (isset($_REQUEST['remove_rss_feed'])) {
                $this->cache['speed_optimization']['remove_rss_feed'] = 1;
            } else {
                $this->cache['speed_optimization']['remove_rss_feed'] = 0;
            }


            if (isset($_REQUEST['clean-cache-frequency'])) {
                $this->cache['speed_optimization']['clean_cache'] = (int)$_REQUEST['clean-cache-frequency'];
            } else {
                $this->cache['speed_optimization']['clean_cache'] = 0;
            }

            $this->cache['speed_optimization']['clean_cache_each_params'] = (int)$_REQUEST['clean-cache-each-params'];

            if (isset($_POST['disable_page'])) {
                $input = $_POST['disable_page'];
                //decode url when insert russian character to input text
                $input = rawurldecode($input);
                $input = trim($input);
                $input = str_replace(' ', '', $input);
                $input = explode("\n", $input);

                $this->cache['speed_optimization']['disable_page'] = $input;
            } else {
                $this->cache['speed_optimization']['disable_page'] = array();
            }

            if (class_exists('WpsolAddonSpeedOptimization')) {
                $this->cache = apply_filters('wpsol_addon_storage_settings', $this->cache, $_REQUEST);
            }

            //disabled cache mobile and tablet when other mobile plugin installed
            if (file_exists(WP_PLUGIN_DIR . '/wp-mobile-detect/wp-mobile-detect.php') ||
                file_exists(WP_PLUGIN_DIR . '/wp-mobile-edition/wp-mobile-edition.php') ||
                file_exists(WP_PLUGIN_DIR . '/wptouch/wptouch.php') ||
                file_exists(WP_PLUGIN_DIR . '/wiziapp-create-your-own-native-iphone-app/wiziapp.php') ||
                file_exists(WP_PLUGIN_DIR . '/wordpress-mobile-pack/wordpress-mobile-pack.php')
            ) {
                $this->cache['speed_optimization']['devices']['cache_tablet'] = 3;
                $this->cache['speed_optimization']['devices']['cache_mobile'] = 3;
            }
            $opts['speed_optimization'] = $this->cache['speed_optimization'];
            update_option('wpsol_optimization_settings', $opts);
        }
    }

    /**
     * Save save_settings_advanced_features
     * @param $opts
     */
    public function saveSettingsAdvancedFeatures($opts)
    {
        if (!empty($opts)) {
            if (isset($_REQUEST['html-minification'])) {
                $this->minification['advanced_features']['html_minification'] = 1;
            } else {
                $this->minification['advanced_features']['html_minification'] = 0;
            }
            if (isset($_REQUEST['css-minification'])) {
                $this->minification['advanced_features']['css_minification'] = 1;
            } else {
                $this->minification['advanced_features']['css_minification'] = 0;
            }
            if (isset($_REQUEST['js-minification'])) {
                $this->minification['advanced_features']['js_minification'] = 1;
            } else {
                $this->minification['advanced_features']['js_minification'] = 0;
            }
            if (isset($_REQUEST['cssgroup-minification'])) {
                $this->minification['advanced_features']['cssgroup_minification'] = 1;
            } else {
                $this->minification['advanced_features']['cssgroup_minification'] = 0;
            }
            if (isset($_REQUEST['jsgroup-minification'])) {
                $this->minification['advanced_features']['jsgroup_minification'] = 1;
            } else {
                $this->minification['advanced_features']['jsgroup_minification'] = 0;
            }

            if (class_exists('WpsolAddonSpeedOptimization')) {
                do_action('wpsol_addon_storage_exclude_file');
                $this->minification = apply_filters('wpsol_addon_storage_settings', $this->minification, $_REQUEST);
            }

            $opts['advanced_features'] = $this->minification['advanced_features'];

            update_option('wpsol_optimization_settings', $opts);
        }
    }

    /**
     * Write gzip htaccess to .htaccess
     * @param $check
     * @return bool
     */
    public static function addGzipHtacess($check)
    {
        $htaccessFile = ABSPATH . DIRECTORY_SEPARATOR . '.htaccess';
        $htaccessContent = '';
        $data = "#WP Speed of Light Gzip compression activation
<IfModule mod_deflate.c>
# Launch the compression
SetOutputFilter DEFLATE
# Force deflate for mangled headers
<IfModule mod_setenvif.c>
<IfModule mod_headers.c>
SetEnvIfNoCase ^(Accept-EncodXng|X-cept-Encoding|X{15}|~{15}|-{15})$";
        $data .= " ^((gzip|deflate)\s*,?\s*)+|[X~-]{4,13}$ HAVE_Accept-Encoding
RequestHeader append Accept-Encoding \"gzip,deflate\" env=HAVE_Accept-Encoding
# Remove non-compressible file type
SetEnvIfNoCase Request_URI \
\.(?:gif|jpe?g|png|rar|zip|exe|flv|mov|wma|mp3|avi|swf|mp?g|mp4|webm|webp)$ no-gzip dont-vary
</IfModule>
</IfModule>

# Regroup compressed resource in MIME-types
<IfModule mod_filter.c>
AddOutputFilterByType DEFLATE application/atom+xml \
		                          application/javascript \
		                          application/json \
		                          application/rss+xml \
		                          application/vnd.ms-fontobject \
		                          application/x-font-ttf \
		                          application/xhtml+xml \
		                          application/xml \
		                          font/opentype \
		                          image/svg+xml \
		                          image/x-icon \
		                          text/css \
		                          text/html \
		                          text/plain \
		                          text/x-component \
		                          text/xml
</IfModule>
<IfModule mod_headers.c>
Header append Vary: Accept-Encoding
</IfModule>
</IfModule>

<IfModule mod_mime.c>
AddType text/html .html_gzip
AddEncoding gzip .html_gzip
</IfModule>
<IfModule mod_setenvif.c>
SetEnvIfNoCase Request_URI \.html_gzip$ no-gzip
</IfModule>
#End of WP Speed of Light Gzip compression activation" . PHP_EOL;
        if ($check) {
            if (!is_super_admin()) {
                return false;
            }
            //open htaccess file

            if (is_writable($htaccessFile)) {
                $htaccessContent = file_get_contents($htaccessFile);
            }

            if (empty($htaccessContent)) {
                return false;
            }
            //if isset Gzip access
            if (strpos($htaccessContent, 'mod_deflate') !== false ||
                strpos($htaccessContent, 'mod_setenvif') !== false ||
                strpos($htaccessContent, 'mod_headers') !== false ||
                strpos($htaccessContent, 'mod_mime') !== false ||
                strpos($htaccessContent, '#WP Speed of Light Gzip compression activation') !== false) {
                return false;
            }

            $htaccessContent = $data . $htaccessContent;
            file_put_contents($htaccessFile, $htaccessContent);
            return true;
        } else {
            if (!is_super_admin()) {
                return true;
            }
            //open htaccess file
            if (is_writable($htaccessFile)) {
                $htaccessContent = file_get_contents($htaccessFile);
            }
            if (empty($htaccessContent)) {
                return false;
            }

            $htaccessContent = str_replace($data, '', $htaccessContent);
            file_put_contents($htaccessFile, $htaccessContent);
            return true;
        }
    }

    /**
     * Write expires header to .htaccess
     * @param $check
     * @return bool
     */
    public static function addExpiresHeader($check)
    {
        $htaccessFile = ABSPATH . DIRECTORY_SEPARATOR . '.htaccess';
        $htaccessContent = '';
        $expires = "#Expires headers configuration added by Speed of Light plugin" . PHP_EOL .
            "<IfModule mod_expires.c>" . PHP_EOL .
            "   ExpiresActive On" . PHP_EOL .
            "   ExpiresDefault A2592000" . PHP_EOL .
            "   ExpiresByType application/javascript \"access plus 7 days\"" . PHP_EOL .
            "   ExpiresByType text/javascript \"access plus 7 days\"" . PHP_EOL .
            "   ExpiresByType text/css \"access plus 7 days\"" . PHP_EOL .
            "   ExpiresByType image/jpeg \"access plus 7 days\"" . PHP_EOL .
            "   ExpiresByType image/png \"access plus 7 days\"" . PHP_EOL .
            "   ExpiresByType image/gif \"access plus 7 days\"" . PHP_EOL .
            "   ExpiresByType image/ico \"access plus 7 days\"" . PHP_EOL .
            "   ExpiresByType image/x-icon \"access plus 7 days\"" . PHP_EOL .
            "   ExpiresByType image/svg+xml \"access plus 7 days\"" . PHP_EOL .
            "   ExpiresByType image/bmp \"access plus 7 days\"" . PHP_EOL .
            "</IfModule>" . PHP_EOL .
            "#End of expires headers configuration" . PHP_EOL;

        if ($check) {
            if (!is_super_admin()) {
                return false;
            }
            //open htaccess file
            if (is_writable($htaccessFile)) {
                $htaccessContent = file_get_contents($htaccessFile);
            }


            if (empty($htaccessContent)) {
                return false;
            }
            //if isset expires header in htacces
            if (strpos($htaccessContent, 'mod_expires') !== false ||
                strpos($htaccessContent, 'ExpiresActive') !== false ||
                strpos($htaccessContent, 'ExpiresDefault') !== false ||
                strpos($htaccessContent, 'ExpiresByType') !== false) {
                return false;
            }

            $htaccessContent = $expires . $htaccessContent;
            file_put_contents($htaccessFile, $htaccessContent);
            return true;
        } else {
            if (!is_super_admin()) {
                return false;
            }
            //open htaccess file
            if (is_writable($htaccessFile)) {
                $htaccessContent = file_get_contents($htaccessFile);
            }
            if (empty($htaccessContent)) {
                return false;
            }
            $htaccessContent = str_replace($expires, '', $htaccessContent);
            file_put_contents($htaccessFile, $htaccessContent);
            return true;
        }
    }
    /**
     * Remove query string from static resources
     * @param $content
     * @return mixed
     */
    public function removeQueryStrings($content)
    {
        if ($content != '') {
            $blog_regexp = self::blogDomainRootUrlRegexp();

            if (!$blog_regexp) {
                return $content;
            }
            $pattern = '~(href|src)=?([\'"])((' .
                $blog_regexp .
                ')?(/[^\'"/][^\'"]*\.([a-z-_]+)([\?#][^\'"]*)?))[\'"]~Ui';
            $content = preg_replace_callback(
                $pattern,
                array($this, 'queryStringsReplaceCallback'),
                $content
            );
        }
        return $content;
    }

    /**
     * Callback replace for js and css file
     * @param $matches
     * @return string
     */
    public function queryStringsReplaceCallback($matches)
    {
        list ($match, $attr, $quote, $url, , , $extension) = $matches;

        if ($extension == 'js' || $extension == 'css') {
            $url = preg_replace('/[&\?]+(ver=([a-z0-9-_\.]+|[0-9-]+))+[&\?]*([a-z0-9-_=]*)*/i', '', $url);
        }
        return $attr . '=' . $quote . $url . $quote;
    }

    /**
     * Returns domain url regexp
     *
     * @return string
     */
    public static function blogDomainRootUrlRegexp()
    {
        $home_url = get_option('home');
        $parse_url = @parse_url($home_url);

        if ($parse_url && isset($parse_url['scheme']) && isset($parse_url['host'])) {
            $scheme = $parse_url['scheme'];
            $host = $parse_url['host'];
            $port = (isset($parse_url['port']) && $parse_url['port'] != 80 ? ':' . (int)$parse_url['port'] : '');
            $domain_url = sprintf('[%s:]*//%s%s', $scheme, $host, $port);

            return $domain_url;
        }

        return false;
    }

    /**
     * Return an instance of the current class, create one if it doesn't exist
     * @since  1.0
     * @return object
     */
    public static function factory()
    {

        static $instance;

        if (!$instance) {
            $instance = new self();
            $instance->setAction();
        }

        return $instance;
    }
}
