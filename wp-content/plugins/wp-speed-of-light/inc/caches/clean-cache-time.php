<?php

//Based on some work of simple-cache
if (!defined('ABSPATH')) exit; // Exit if accessed directly

class WpsolCleanCacheTime
{
    /**
     * WpsolCleanCacheTime constructor.
     */
    public function __construct()
    {
    }

    public function setAction()
    {
        add_action('wpsol_purge_cache', array($this, 'purgeCache'));
        add_action('init', array($this, 'scheduleEvents'));
        add_filter('cron_schedules', array($this, 'filterCronSchedules'));
    }

    /**
     * set up schedule_events
     */
    public function scheduleEvents()
    {
        $config = get_option('wpsol_optimization_settings');
        $timestamp = wp_next_scheduled('wpsol_purge_cache');
        // Expire cache never
        if (isset($config['speed_optimization']['clean_cache']) && $config['speed_optimization']['clean_cache'] === 0) {
            wp_unschedule_event($timestamp, 'wpsol_purge_cache');
            return;
        }
        if (!$timestamp) {
            wp_schedule_event(time(), 'wpsol_cache', 'wpsol_purge_cache');
        }
    }

    /**
     *  Unschedule events
     */
    public function unscheduleEvents()
    {
        $timestamp = wp_next_scheduled('wpsol_purge_cache');
        wp_unschedule_event($timestamp, 'wpsol_purge_cache');
    }

    /**
     * Add custom cron schedule
     */
    public function filterCronSchedules($schedules)
    {
        $config = get_option('wpsol_optimization_settings');
        $cache_frequency = $config['speed_optimization']['clean_cache'];
        $params = $config['speed_optimization']['clean_cache_each_params'];
        $interval = HOUR_IN_SECONDS;
        if (!empty($cache_frequency) && $cache_frequency > 0) {
            // check parameter
            if ($params == 0) {
                $interval = $cache_frequency * DAY_IN_SECONDS;
            } elseif ($params == 1) {
                $interval = $cache_frequency * HOUR_IN_SECONDS;
            } else {
                $interval = $cache_frequency * MINUTE_IN_SECONDS;
            }
        }
        $schedules['wpsol_cache'] = array(
            'interval' => apply_filters('wpsol_cache_purge_interval', $interval),
            'display' => esc_html__('WPSOL Cache Purge Interval', 'wp-speed-of-light'),
        );
        return $schedules;
    }

    /**
     *   A Cache purse
     */
    public function purgeCache()
    {
        $config = get_option('wpsol_optimization_settings');
        // Do nothing, caching is turned off
        if (empty($config['speed_optimization']['act_cache'])) {
            return;
        }
        WpsolCache::factory()->wpsolCacheFlush();
        WpsolMinificationCache::clearMinification();
    }

    /**
     * @return WpsolCleanCacheTime
     */
    public static function factory()
    {
        static $instance;
        if (!$instance) {
            $instance = new self();
            $instance->setAction();
        }
        return $instance;
    }
}
