<?php
if (!defined('ABSPATH')) exit;

class WpsolDashboard
{

    public $cache = 0;
    public $minification = 0;

    /**
     * WpsolDashboard constructor.
     */
    public function __construct()
    {
    }

    /**
     * Analysis system for display dashboard
     * @return array
     */
    public function dashboard()
    {
        $plugin_enable = array();
        $results = array('cache' => 0,
            'minification' => 0,
            'group_resources' => 0,
            'database-clean' => 0,
            'extension' => array(
                'load-time' => 0,
                'first-byte' => 0,
                'render' => 0
            ),
            'queries' => array(
                'plugin' => 0,
                'themes' => 0,
                'wp-core' => 0
            ),
            'plugins_enable' => 0,
            'plugins_disable' => 0
        );

        $optimization = get_option('wpsol_optimization_settings');
        if (!empty($optimization)) {
            if ($optimization['speed_optimization']['act_cache'] == 1) {
                $results['cache'] = $this->cache + 50;
            }

            $headers = self::getHeadersResponse();
            if (isset($headers['Content-Encoding']) && $headers['Content-Encoding'] == 'gzip') {
                $results['cache'] = $results['cache'] + 50;
            }
            if ($optimization['advanced_features']['html_minification'] == 1) {
                $results['minification'] = $this->minification + 33;
            }
            if ($optimization['advanced_features']['css_minification'] == 1) {
                $results['minification'] = $results['minification'] + 33;
            }
            if ($optimization['advanced_features']['js_minification'] == 1) {
                $results['minification'] = $results['minification'] + 33;
            }
            if ($optimization['advanced_features']['js_minification'] == 1 &&
                $optimization['advanced_features']['css_minification'] == 1 &&
                $optimization['advanced_features']['html_minification'] == 1) {
                $results['minification'] = $results['minification'] + 1;
            }

            if ($optimization['advanced_features']['cssgroup_minification'] == 1) {
                $results['group_resources'] = $results['group_resources'] + 50;
            }
            if ($optimization['advanced_features']['jsgroup_minification'] == 1) {
                $results['group_resources'] = $results['group_resources'] + 50;
            }
        }
        $database = get_option('wpsol_clean_database_config');
        $arr = array();
        if (!empty($database)) {
            foreach ($database as $k => $v) {
                if ($v != 'transient') {
                    $arr[] = $v;
                }
            }
            for ($i = 0; $i <= count($arr); $i++) {
                $results['database-clean'] = (int)round($i * 10);
            }
        }

        //lastest page test
        $extension = get_option('wpsol_loadtime_lastest');
        if (!empty($extension)) {
            $results['extension']['load-time'] = $extension['first']['load-time'];
            $results['extension']['first-byte'] = $extension['first']['first-byte'];
            $results['extension']['render'] = $extension['first']['render'];
        }
        $queries = get_option('wpsol_database_queries_analysis');
        if (!empty($queries)) {
            $results['queries']['plugin'] = $queries['plugin']['load_time'];
            $results['queries']['themes'] = $queries['theme']['load_time'];
            $results['queries']['wp-core'] = $queries['core']['load_time'];
        }
        $plugins = get_plugins();
        $plugins_disable = array();
        foreach ($plugins as $k => $v) {
            if (is_plugin_active($k) == true) {
                $plugin_enable[] = $k;
            }
            if (is_plugin_active($k) == false) {
                $plugins_disable[] = $k;
            }
        }
        $results['plugins_enable'] = count($plugin_enable);
        $results['plugins_disable'] = count($plugins_disable);

        return $results;
    }

    /*
     * Check gzip activeed
     */
    public static function checkGzipActivated()
    {
        if (!current_user_can('manage_options')) {
            wp_send_json(false);
        }
        $headers = self::getHeadersResponse();
        if (isset($headers['Content-Encoding']) && $headers['Content-Encoding'] == 'gzip') {
            wp_send_json(true);
        }
        wp_send_json(false);
    }

    /**
     * Get header response
     * @return array|Requests_Utility_CaseInsensitiveDictionary
     */
    public static function getHeadersResponse()
    {
        $url = home_url();
        $args = array(
            'headers' => array(
                'timeout' => 30,
                'redirection' => 10,
            )
        );
        // Retrieve the raw response from the HTTP request
        $response = wp_remote_get($url, $args);
        $headers = wp_remote_retrieve_headers($response);

        return $headers;
    }
}
