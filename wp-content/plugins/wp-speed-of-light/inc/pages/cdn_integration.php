<?php
$cdn_integration = get_option('wpsol_cdn_integration');
$cdn_content_value = '';
$cdn_exclude_content_value = '';
if (!empty($cdn_integration['cdn_content'])) {
    $cdn_content_value = implode(',', $cdn_integration['cdn_content']);
}
if (!empty($cdn_integration['cdn_exclude_content'])) {
    $cdn_exclude_content_value = implode(',', $cdn_integration['cdn_exclude_content']);
}
?>
<h1><label><?php _e('WP Speed of Light CDN Integration', 'wp-speed-of-light') ?></label></h1>
<?php if (isset($_REQUEST['save-settings']) && $_REQUEST['save-settings'] == 'success') : ?>
    <div id="message-save-settings" class="notice notice-success" style="margin:15px 0; padding: 10px;">
        <strong><?php _e('Setting saved', 'wp-speed-of-light'); ?></strong></div>
<?php endif; ?>
<div id="wpsol-cdn-integration">
    <form method="post" action="">
        <input type="hidden" name="action" value="wpsol_save_cdn">
        <?php wp_nonce_field('wpsol_cdn_integration', '_wpsol_cdn_nonce'); ?>
        <table cellspacing="20">
            <tr>
                <td>
                    <label for="cdn-active" class="speedoflight_tool cdn_label"
                           alt="<?php _e('Enable to make CDN effective on your website', 'wp-speed-of-light') ?>">
                        <?php _e('Activate CDN', 'wp-speed-of-light') ?></label>
                </td>
                <td>
                    <div class="switch-optimization">
                        <label class="switch switch-optimization">
                            <input type="checkbox" class="cdn-active" id="cdn-active" name="cdn-active"
                                   value="1" <?php checked($cdn_integration['cdn_active'], '1') ?>>
                            <div class="slider round"></div>
                        </label>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="cdn-url" class="speedoflight_tool cdn_label"
                           alt="<?php _e('Add your CDN URL, without 
                           the trailing slash (at the end)', 'wp-speed-of-light') ?>">
                        <?php _e('CDN URL', 'wp-speed-of-light') ?></label>
                </td>
                <td>
                    <input type="text" id="cdn-url" name="cdn-url" size="50"
                           placeholder="<?php _e('https://www.domain.com', 'wp-speed-of-light') ?>"
                           value="<?php
                                echo(($cdn_integration['cdn_url']) ?
                                    esc_html($cdn_integration['cdn_url']) :
                                    ''); ?>"/>
                    <br>
                    <label class="cdn_tool_tip"><b>Note:&nbsp;</b>
                        <?php _e('Use double slash ‘//’ at the start or url, 
                    if you have some pages on  HTTP and some are on HTTPS.', 'wp-speed-of-light') ?>
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="cdn-content" class="speedoflight_tool cdn_label"
                           alt="<?php _e('Your WordPress content served through CDN resources, separated by comma.
                           By default wp-content,wp-includes', 'wp-speed-of-light') ?>">
                        <?php _e('CDN Content', 'wp-speed-of-light') ?></label>
                </td>
                <td>
                    <input type="text" id="cdn-content" name="cdn-content" size="50"
                           value="<?php echo(($cdn_content_value) ? esc_html($cdn_content_value) : ''); ?>"/>
                    <br>
                    <label class="cdn_tool_tip"><b>Note:&nbsp;</b>
                        <?php _e('Enter the directories (comma separated) of which you want the CDN 
                    to serve the content.', 'wp-speed-of-light') ?>
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="cdn-exclude-content" class="speedoflight_tool cdn_label"
                           alt="<?php _e('Exclude file type or directories from CDN network', 'wp-speed-of-light') ?>">
                        <?php _e('Exclude Content', 'wp-speed-of-light') ?></label>
                </td>
                <td>
                    <input type="text" id="cdn-exclude-content" name="cdn-exclude-content" size="50"
                           value="<?php
                             echo(($cdn_exclude_content_value) ?
                               esc_html($cdn_exclude_content_value) :
                               ''); ?>
                        "/>
                    <br>
                    <label class="cdn_tool_tip"><b>Note:&nbsp;</b>
                        <?php _e('Exclude file types or directories from CDN. Example,
                     enter .css to exclude the CSS files.', 'wp-speed-of-light') ?>
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="cdn-relative-path" class="speedoflight_tool cdn_label"
                           alt="<?php _e('Enabled by default, Enable/Disable the CDN for relative paths resources.
                     Used for some compatibilities with specific Wordpress plugins', 'wp-speed-of-light') ?>">
                        <?php _e('Relative path', 'wp-speed-of-light') ?>
                    </label>
                </td>
                <td>
                    <div class="switch-optimization">
                        <label class="switch switch-optimization">
                            <input type="checkbox" class="cdn-relative-path" id="cdn-relative-path"
                                   name="cdn-relative-path"
                                   value="1" <?php checked($cdn_integration['cdn_relative_path'], '1') ?>>
                            <div class="slider round"></div>
                        </label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php
                    do_action('wpsol_addon_add_clean_3rd_party_cache', $cdn_integration);
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="<?php _e('Save settings', 'wp-speed-of-light'); ?>"
                           class="btn waves-effect waves-light" id="cdn-integration-button"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php
                    if (!class_exists('WpsolAddonCDNIntegration')) :
                        ?>
                        <div class="card wpsol_addon_notice light-blue notice notice-success is-dismissible below-h2">
                            <?php echo __('Clean up third party CDN like Cloudflare, MaxCDN, 
                            KeyCDN, Varnish, Siteground in the optional ', 'wp-speed-of-light');?>
                            <a class="pro-addon" href="https://www.joomunited.com/wordpress-products/wp-speed-of-light"
                               target="_blank">
                                <?php echo __('PRO ADDON', 'wp-speed-of-light');?></a>

                        </div>
                        <?php
                    endif;
                    ?>
                </td>
            </tr>
        </table>

    </form>
</div>

