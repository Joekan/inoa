<?php
$config = get_option('wpsol_configuration');

?>
<h1 class="wpsol-configuration-title"><label><?php _e('WP Speed of Light settings', 'wp-speed-of-light') ?></label></h1>

<?php if (isset($_REQUEST['save-settings']) && $_REQUEST['save-settings'] == 'success') : ?>
    <div id="message-save-settings" class="notice notice-success" style="margin-top:20px; padding: 10px;">
        <strong><?php _e('Setting saved', 'wp-speed-of-light'); ?></strong></div>
<?php endif; ?>

<div id="tabs-wpsol-settings">
    <ul class="tabs z-depth-1 cyan">
        <li class="tab" id="wpsol-configuration"><a data-tab-id="tabConfiguration" id="tab-tabConfiguration"
                                                    class="link-tab white-text waves-effect waves-light"
                                                    href="#tab1">
        <?php _e('General Settings', 'wp-speed-of-light') ?></a>
        </li>
        <li class="tab" id="wpsol-translation"><a data-tab-id="tabTranslation" id="tab-tabTranslation"
                                                  class="link-tab white-text waves-effect waves-light"
                                                  href="#tab2"><?php _e('Translation tool', 'wp-speed-of-light') ?></a>
        </li>
    </ul>

    <div>
        <form method="POST">
            <input type="hidden" name="action" value="wpsol_configuration">
            <?php wp_nonce_field('wpsol_save_parameter', '_wpsol_nonce'); ?>
            <div id="tab1" class="tab-content cyan lighten-4 active">
                <div class="wpsol-configuration-tab">
                    <ul class="wpsol-configuration-list">
                        <li>
                            <label for="config2" class="speedoflight_tool wpsol-label-configuration"
                                   style="vertical-align: middle;"
                                   alt="<?php _e('Display a button in the topbar 
                                   to clean all the site cache', 'wp-speed-of-light') ?>">
                                <?php _e('Display clean cache in top toolbar', 'wp-speed-of-light'); ?></label>
                            <div class="switch-optimization wpsol-option-configuration">
                                <label class="switch ">
                                    <input type="checkbox" class="wpsol-optimization wpsol-configuration" id="config2"
                                           name="display_clean"
                                           value="<?php echo $config['display_clean']; ?>"
                                        <?php
                                        if (!empty($config)) {
                                            if ($config['display_clean'] == 1) {
                                                echo 'checked="checked"';
                                            }
                                        } ?>
                                    />
                                    <div class="slider round"></div>
                                </label>
                            </div>
                        </li>
                        <li>
                            <label for="config1" class="speedoflight_tool wpsol-label-configuration"
                                   alt="<?php _e('When an admin is Logged In you can disable 
                                   speed optimization (cache, compression…)', 'wp-speed-of-light') ?>">
                                <?php _e('Disable optimization for admin users', 'wp-speed-of-light'); ?></label>
                            <div class="switch-optimization wpsol-option-configuration">
                                <label class="switch ">
                                    <input type="checkbox" class="wpsol-optimization" id="config1" name="disable_user"
                                           value="<?php echo $config['disable_user']; ?>"
                                        <?php
                                        if (!empty($config)) {
                                            if ($config['disable_user'] == 1) {
                                                echo 'checked="checked"';
                                            }
                                        }
                                        ?>
                                    />
                                    <div class="slider round"></div>
                                </label>
                            </div>
                        </li>
                        <?php
                        do_action('wpsol_addon_add_disable_optimize_for_user_roles', $config);
                        ?>
                        <li>
                            <label for="config3" class="speedoflight_tool" style="vertical-align: baseline;"
                                   alt="<?php _e('WebPageTest API key required to run the speed analysis tool. 
                                   Allows 200 page speed test per day.
                                   Hit the button to get one, it’s FREE!', 'wp-speed-of-light') ?>">
                                <?php _e('Your WebPagetest API Key :', 'wp-speed-of-light'); ?></label>
                            <input type="text" size="64" id="config3" class="wpsol-configuration" name="webtest_api_key"
                                   style="margin-left:30px;"
                                   value="<?php
                                    if (!empty($config['webtest_api_key'])) {
                                        echo $config['webtest_api_key'];
                                    } else {
                                        echo "" ;
                                    } ?>"
                            />
                        </li>
                        <li>
                            <input type="button" class="button action"
                                   value="<?php _e('Get an API key', 'wp-speed-of-light'); ?>"
                                   data-url="https://www.webpagetest.org/getkey.php" id="get-api-button"/>
                        </li>

                    </ul>
                    <input type="submit" value="<?php _e('Save settings', 'wp-speed-of-light'); ?>"
                           class="save-settings btn waves-effect waves-light"/>
                    <input type="button" value="<?php _e('Clean Cache', 'wp-speed-of-light'); ?>"
                           class="btn waves-effect waves-light clean-cache-button" name="clean-cache-button"/>
                    <?php
                    if (!class_exists('WpsolAddonConfiguration')) :
                        ?>
                        <div class="card wpsol_addon_notice light-blue notice notice-success is-dismissible below-h2">
                            <?php echo __('Cache exclusion rule, disable cache per user role 
                            and more  options in the optional ', 'wp-speed-of-light'); ?>
                            <a class="pro-addon" href="https://www.joomunited.com/wordpress-products/wp-speed-of-light"
                               target="_blank"><?php echo __('PRO ADDON', 'wp-speed-of-light'); ?></a>
                        </div>
                        <?php
                    endif;
                    ?>
                </div>
            </div>
        </form>
        <div id="tab2" class="tab-content cyan lighten-4">
            <div class="content-optimization">
                <?php echo \Joomunited\WPSOL\Jutranslation\Jutranslation::getInput(); ?>
            </div>
        </div>
    </div>
</div>