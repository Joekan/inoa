<?php
$optimization = get_option('wpsol_optimization_settings');

//disabled cache mobile and tablet when other mobile plugin installed
$disabled = '';
if (file_exists(WP_PLUGIN_DIR . '/wp-mobile-detect/wp-mobile-detect.php') ||
    file_exists(WP_PLUGIN_DIR . '/wp-mobile-edition/wp-mobile-edition.php') ||
    file_exists(WP_PLUGIN_DIR . '/wptouch/wptouch.php') ||
    file_exists(WP_PLUGIN_DIR . '/wiziapp-create-your-own-native-iphone-app/wiziapp.php') ||
    file_exists(WP_PLUGIN_DIR . '/wordpress-mobile-pack/wordpress-mobile-pack.php')
) {
    $disabled = 'disabled';
}
$parameters = array('Days', 'Hours', 'Minutes');

?>
<?php if (isset($_REQUEST['save-settings']) && $_REQUEST['save-settings'] == 'success') : ?>
    <div id="message-save-settings" class="notice notice-success" style="margin: 10px 0 10px 0;padding: 10px;">
        <strong><?php _e('Optimization settings saved', 'wp-speed-of-light'); ?></strong></div>
<?php endif; ?>
<div id="tabs-analysis">
    <ul class="tabs z-depth-1 cyan">
        <li class="tab" id="wpsol-speed-optimization"><a data-tab-id="tabSpeedOptimization"
                                                         id="tab-tabSpeedOptimization"
                                                         class="link-tab white-text waves-effect waves-light"
                                                         href="#tab1">
                <?php _e('Speed optimization', 'wp-speed-of-light') ?></a>
        </li>
        <li class="tab" id="wpsol-advanced-features"><a data-tab-id="tabAdvancedFeatures" id="tab-tabAdvancedFeatures"
                                                        class="link-tab white-text waves-effect waves-light"
                                                        href="#tab2">
                <?php _e('Minify & Group Resources', 'wp-speed-of-light') ?></a>
        </li>
        <?php
        if (class_exists('WpsolAddonSpeedOptimization')) :
            ?>
            <li class="tab" id="wpsol-advanced-optimization"><a data-tab-id="tabAdvancedOptimization"
                                                                id="tab-tabAdvancedOptimization"
                                                                class="link-tab white-text waves-effect waves-light"
                                                                href="#tab3">
                    <?php _e('Advanced optimization', 'wp-speed-of-light') ?></a>
            </li>
        <?php endif; ?>
    </ul>

    <div class="">
        <div id="tab1" class="tab-content cyan lighten-4 active">
            <div class="content-optimization wpsol-optimization">
                <form class="" method="post" action="">
                    <input type="hidden" name="action" value="wpsol_save_settings">
                    <?php wp_nonce_field('wpsol_speed_optimization', '_wpsol_nonce'); ?>
                    <ul class="field">
                        <li>
                            <label for="active-cache" class="text speedoflight_tool"
                                   alt="<?php _e('Cache activation will speedup your website by pre-loading
                                    common page elements and database queries', 'wp-speed-of-light') ?>">
                                <?php _e('Activate cache system', 'wp-speed-of-light') ?></label>
                            <div class="switch-optimization">
                                <label class="switch switch-optimization">
                                    <input type="checkbox" class="wpsol-optimization" id="active-cache"
                                           name="active-cache"
                                           value="<?php echo $optimization['speed_optimization']['act_cache']; ?>"
                                        <?php
                                        if (!empty($optimization)) {
                                            if ($optimization['speed_optimization']['act_cache'] == 1) {
                                                echo 'checked="checked"';
                                            }
                                        }?> >
                                    <div class="slider round"></div>
                                </label>
                            </div>
                        </li>
                        <li>
                            <label for="clean-cache-frequency" class="text speedoflight_tool"
                                   alt="<?php _e('Automatically cleanup the cache stored each x minutes and
                                    generate a new version instantly', 'wp-speed-of-light') ?>">
                                <?php _e('Clean each', 'wp-speed-of-light') ?></label>
                            <div class="switch-optimization">
                                <input type="text" class="wpsol-optimization" id="clean-cache-frequency" size="2"
                                       name="clean-cache-frequency"
                                       value="<?php echo $optimization['speed_optimization']['clean_cache']; ?>">
                                <select name="clean-cache-each-params"
                                        style="vertical-align: baseline;display: inline-block">'
                                    <?php
                                    foreach ($parameters as $k => $v) {
                                        $selected = '';
                                        if ($k == $optimization['speed_optimization']['clean_cache_each_params']) {
                                            $selected = 'selected = "selected"';
                                        }
                                        echo '<option value="' . $k . '" ' . $selected . '>' . $v . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </li>
                        <?php
                        if (class_exists('WpsolAddonSpeedOptimization')) {
                            do_action('wpsol_addon_add_auto_clean_on_save', $optimization);
                        } else {
                            ?>

                            <?php
                        }
                        ?>
                        <li>
                            <label for="cache-desktop" class="text speedoflight_tool"
                                   alt="<?php _e('Serve the cache for desktop for all devices: Recommended,
                                    unless wrong cache version is served', 'wp-speed-of-light') ?>">
                                <?php _e('Cache for desktop', 'wp-speed-of-light') ?></label>
                            <div class="switch-optimization">
                                <select id="cache-desktop" name="cache-desktop">
                                    <option value="1"
                                        <?php
                                        echo ($optimization['speed_optimization']['devices']['cache_desktop'] == 1) ?
                                            'selected="selected"' :
                                            '' ?>>
                                        <?php _e('Activated', 'wp-speed-of-light') ?></option>
                                    <option value="2"
                                        <?php
                                        echo ($optimization['speed_optimization']['devices']['cache_desktop'] == 2) ?
                                            'selected="selected"' :
                                            '' ; ?>>
                                        <?php _e('No cache for desktop', 'wp-speed-of-light') ?></option>
                                </select>

                            </div>
                        </li>
                        <li>
                            <label for="cache-tablet" class="text speedoflight_tool"
                                   alt="<?php _e('Serve the cache for tablet: Recommended ONLY if you’re 
                                   experiencing wrong cache version served', 'wp-speed-of-light') ?>">
                                <?php _e('Cache for tablet', 'wp-speed-of-light') ?></label>
                            <div class="switch-optimization">
                                <select id="cache-tablet" name="cache-tablet" <?php echo $disabled; ?>>
                                    <option value="1"
                                        <?php
                                        if ($optimization['speed_optimization']['devices']['cache_tablet'] == 1) {
                                            echo 'selected="selected"';
                                        }  ?>>
                                        <?php _e('Automatic (same as desktop)', 'wp-speed-of-light') ?></option>
                                    <option value="2"
                                        <?php
                                        if ($optimization['speed_optimization']['devices']['cache_tablet'] == 2) {
                                            echo 'selected="selected"';
                                        } ?>>
                                        <?php _e('Specific tablet cache', 'wp-speed-of-light') ?></option>
                                    <option value="3"
                                        <?php
                                        if ($optimization['speed_optimization']['devices']['cache_tablet'] == 3) {
                                            echo 'selected="selected"';
                                        } ?>>
                                        <?php _e('No cache for tablet', 'wp-speed-of-light') ?></option>
                                </select>

                            </div>
                        </li>
                        <li>
                            <label for="cache-mobile" class="text speedoflight_tool"
                                   alt="<?php _e('Serve the cache for mobile: Recommended ONLY if you’re experiencing 
                                   wrong cache version served', 'wp-speed-of-light') ?>">
                                <?php _e('Cache for mobile', 'wp-speed-of-light') ?></label>
                            <div class="switch-optimization">
                                <select id="cache-mobile" name="cache-mobile" <?php echo $disabled; ?>>
                                    <option value="1"
                                        <?php
                                        echo ($optimization['speed_optimization']['devices']['cache_mobile'] == 1)
                                            ? 'selected="selected"'
                                            : '' ?>>
                                        <?php _e('Automatic (same as desktop)', 'wp-speed-of-light') ?></option>
                                    <option value="2"
                                        <?php
                                        echo ($optimization['speed_optimization']['devices']['cache_mobile'] == 2)
                                            ? 'selected="selected"'
                                            : '' ?>>
                                        <?php _e('Specific mobile cache', 'wp-speed-of-light') ?></option>
                                    <option value="3"
                                        <?php
                                        echo ($optimization['speed_optimization']['devices']['cache_mobile'] == 3) ?
                                            'selected="selected"' :
                                            '' ?>>
                                        <?php _e('No cache for mobile', 'wp-speed-of-light') ?></option>
                                </select>

                            </div>
                        </li>
                        <li>
                            <label for="query-strings" class="text speedoflight_tool"
                                   alt="<?php _e('Remove query strings from static resources like 
                                   CSS & JS files inside the Header to 
                                   improve your scores on services like Pingdom,
                                    GTmetrix, PageSpeed and YSlow', 'wp-speed-of-light') ?>">
                                <?php _e('Remove query strings', 'wp-speed-of-light') ?></label>
                            <div class="switch-optimization">
                                <label class="switch ">
                                    <input type="checkbox" class="wpsol-optimization" id="query-strings"
                                           name="query-strings"
                                           value="<?php echo $optimization['speed_optimization']['query_strings']; ?>"
                                        <?php
                                        if (!empty($optimization)) {
                                            if ($optimization['speed_optimization']['query_strings'] == 1) {
                                                echo 'checked="checked"';
                                            }
                                        }
                                        ?>
                                    >
                                    <div class="slider round"></div>
                                </label>
                            </div>
                        </li>
                        <li>
                            <label for="add-expires" class="text speedoflight_tool"
                                   alt="<?php _e("Expires headers gives instruction 
                                   to the browser whether it should request a specific file
                                    from the server or whether they should grab it 
                                    from the browser's cache (it’s faster).", 'wp-speed-of-light') ?>">
                                <?php _e('Add expire headers', 'wp-speed-of-light') ?></label>
                            <div class="switch-optimization">
                                <label class="switch ">
                                    <input type="checkbox" class="wpsol-optimization" id="add-expires"
                                           name="add-expires"
                                           value="<?php echo $optimization['speed_optimization']['add_expires']; ?>"
                                        <?php
                                        if (!empty($optimization)) {
                                            if ($optimization['speed_optimization']['add_expires'] == 1) {
                                                echo 'checked="checked"';
                                            }
                                        }
                                        ?>
                                    >
                                    <div class="slider round"></div>
                                </label>
                            </div>
                        </li>
                        <li>
                            <label for="cache-external" class="text speedoflight_tool"
                                   alt="<?php _e('Cache external resources such as script served from Google. Warning:
                                    make sure you monitor the performance before and after activation, in some case you may
                                     experience performance loss when activated!', 'wp-speed-of-light') ?>">
                                <?php _e('Cache external script', 'wp-speed-of-light'); ?></label>
                            <div class="switch-optimization">
                                <label class="switch ">
                                    <input type="checkbox" class="wpsol-optimization" id="cache-external"
                                           name="cache_external_script"
                                           value="<?php echo $optimization['speed_optimization']['cache_external_script']; ?>"
                                        <?php
                                        if (!empty($optimization)) {
                                            if ($optimization['speed_optimization']['cache_external_script'] == 1) {
                                                echo 'checked="checked"';
                                            }
                                        } ?>
                                    />
                                    <div class="slider round"></div>
                                </label>
                            </div>
                        </li>
                        <li>
                            <label for="remove-rest" class="text speedoflight_tool"
                                   alt="<?php _e('Disable the WordPress REST API (API to retrieve data using 
                                   GET requests, used by developers)', 'wp-speed-of-light') ?>">
                                <?php _e('Disable REST API', 'wp-speed-of-light'); ?></label>
                            <div class="switch-optimization">
                                <label class="switch ">
                                    <input type="checkbox" class="wpsol-optimization" id="remove-rest"
                                           name="remove_rest_api"
                                           value="<?php echo $optimization['speed_optimization']['remove_rest_api']; ?>"
                                        <?php
                                        if (!empty($optimization)) {
                                            if ($optimization['speed_optimization']['remove_rest_api'] == 1) {
                                                echo 'checked="checked"';
                                            }
                                        } ?>
                                    />
                                    <div class="slider round"></div>
                                </label>
                            </div>
                        </li>
                        <li>
                            <label for="remove-rss" class="text speedoflight_tool"
                                   alt="<?php _e('Disable the WordPress RSS feed. RSS feeds allow users to 
                                   subscribe to your blog posts using an RSS feed reader', 'wp-speed-of-light') ?>">
                                <?php _e('Disable RSS feed', 'wp-speed-of-light'); ?></label>
                            <div class="switch-optimization">
                                <label class="switch ">
                                    <input type="checkbox" class="wpsol-optimization" id="remove-rss"
                                           name="remove_rss_feed"
                                           value="<?php echo $optimization['speed_optimization']['remove_rss_feed']; ?>"
                                        <?php
                                        if (!empty($optimization)) {
                                            if ($optimization['speed_optimization']['remove_rss_feed'] == 1) {
                                                echo 'checked="checked"';
                                            }
                                        } ?>
                                    />
                                    <div class="slider round"></div>
                                </label>
                            </div>
                        </li>
                        <li>
                            <label for="disable-page" class="text speedoflight_tool"
                                   alt="<?php _e('Add the URL of the pages you want to exclude
                                    from cache (one URL per line)', 'wp-speed-of-light') ?>">
                                <?php _e('Never cache the following pages : ', 'wp-speed-of-light'); ?></label>
                            <p><textarea cols="100" rows="7" id="disable-page" class="wpsol-optimization"
                                 name="disable_page"><?php
                                    if (!empty($optimization['speed_optimization']['disable_page'])) {
                                        $output = implode("\n", $optimization['speed_optimization']['disable_page']);
                                        echo $output;
                                    } ?></textarea></p>
                        </li>
                    </ul>
                    <input type="submit" value="<?php _e('Save settings', 'wp-speed-of-light'); ?>"
                           class="btn waves-effect waves-light" id="speed-optimization"/>
                </form>
                <?php
                if (!class_exists('WpsolAddonSpeedOptimization')) :
                    ?>
                    <div class="card wpsol_addon_notice light-blue notice notice-success is-dismissible below-h2">
                        <?php echo __('Cache preloading, font optimization, database auto cleanup 
                        and more options in the optional ', 'wp-speed-of-light'); ?>
                        <a class="pro-addon" href="https://www.joomunited.com/wordpress-products/wp-speed-of-light"
                           target="_blank"><?php echo __('PRO ADDON', 'wp-speed-of-light'); ?></a>
                    </div>
                    <?php
                endif;
                ?>
            </div>
        </div>

        <div id="tab2" class="tab-content cyan lighten-4">
            <div class="content-optimization wpsol-optimization">
                <form class="" method="post" id="speed-optimization-form">
                    <input type="hidden" name="action" value="wpsol_save_minification">
                    <?php wp_nonce_field('wpsol_minify', '_wpsol_nonce'); ?>
                    <div class="text-intro">
                        <blockquote>
                            <i><?php _e('This features may not be applicable to all WordPress websites depending of
                             of the server and the plugin used. Please run a full test on your
                              website before considering keeping those options to Yes.', 'wp-speed-of-light') ?></i>
                        </blockquote>
                    </div>
                    <ul class="field">
                        <li>
                            <label for="html-minification" class="text speedoflight_tool"
                                   alt="<?php _e('Minification refers to the process
                                    of removing unnecessary or redundant data
                                    without affecting how the resource is processed
                                     by the browser - e.g. code comments and formatting,
                                     removing unused code, using shorter variable
                                      and function names, and so on', 'wp-speed-of-light') ?>">
                                <?php _e('HTML minification', 'wp-speed-of-light') ?></label>
                            <div class="switch-optimization">
                                <label class="switch switch-optimization">
                                    <input type="checkbox" class="wpsol-minification" id="html-minification"
                                           name="html-minification"
                                       value="<?php echo $optimization['advanced_features']['html_minification']; ?>"
                                        <?php
                                        if (!empty($optimization) &&
                                            $optimization['advanced_features']['html_minification'] == 1) {
                                            echo 'checked="checked"';
                                        }
                                        ?>>
                                    <div class="slider round"></div>
                                </label>
                            </div>
                        </li>
                        <li>
                            <label for="css-minification" class="text speedoflight_tool"
                            alt="<?php _e('Minification refers to the process of removing unnecessary or redundant data
                            without affecting how the resource is processed 
                            by the browser - e.g. code comments and formatting, 
                            removing unused code, using shorter variable and
                             function names, and so on', 'wp-speed-of-light') ?>">
                                <?php _e('CSS minification', 'wp-speed-of-light') ?></label>
                            <div class="switch-optimization">
                                <label class="switch ">
                                    <input type="checkbox" class="wpsol-minification" id="css-minification"
                                           name="css-minification"
                                           value="<?php echo $optimization['advanced_features']['css_minification']; ?>"
                                        <?php
                                        if (!empty($optimization) &&
                                            $optimization['advanced_features']['css_minification'] == 1) {
                                            echo 'checked="checked"';
                                        }
                                        ?>
                                    >
                                    <div class="slider round"></div>
                                </label>
                            </div>
                        </li>
                        <li>
                            <label for="js-minification" class="text speedoflight_tool"
                                   alt="<?php _e('Minification refers to the process of removing unnecessary 
                                   or redundant data without affecting how the resource 
                                   is processed by the browser - e.g. code comments and 
                                   formatting, removing unused code,
                                   using shorter variable and function names, and so on', 'wp-speed-of-light') ?>">
                                <?php _e('JS minification', 'wp-speed-of-light') ?></label>
                            <div class="switch-optimization">
                                <label class="switch ">
                                    <input type="checkbox" class="wpsol-minification" id="js-minification"
                                           name="js-minification"
                                           value="<?php echo $optimization['advanced_features']['js_minification']; ?>"
                                        <?php
                                        if (!empty($optimization) &&
                                            $optimization['advanced_features']['js_minification'] == 1) {
                                            echo 'checked="checked"';
                                        }
                                        ?>
                                    >
                                    <div class="slider round"></div>
                                </label>
                            </div>
                        </li>
                        <li>
                            <label for="cssGroup-minification" class="text speedoflight_tool"
                           alt="<?php _e('Grouping several CSS files into a single file will
                            minimize the HTTP requests number.
                            Use with caution and test your website,
                             it may generates conflicts', 'wp-speed-of-light') ?>">
                                <?php _e('Group CSS', 'wp-speed-of-light') ?></label>
                            <div class="switch-optimization">
                                <label class="switch ">
                                    <input type="checkbox" class="wpsol-minification" id="cssGroup-minification"
                                           name="cssgroup-minification"
                                           value="<?php echo $optimization['advanced_features']['js_minification']; ?>"
                                        <?php
                                        if (!empty($optimization) &&
                                            $optimization['advanced_features']['cssgroup_minification'] == 1) {
                                            echo 'checked="checked"';
                                        }
                                        ?>>
                                    <div class="slider round"></div>
                                </label>
                            </div>
                        </li>
                        <li>
                            <label for="jsGroup-minification" class="text speedoflight_tool"
                            alt="<?php _e('Grouping several Javascript files into a single file will minimize the HTTP
                            requests number.Use with caution and test your website,
                             it may generates conflicts', 'wp-speed-of-light') ?>">
                                <?php _e('Group JS', 'wp-speed-of-light') ?></label>
                            <div class="switch-optimization">
                                <label class="switch ">
                                    <input type="checkbox" class="wpsol-minification" id="jsGroup-minification"
                                           name="jsgroup-minification"
                                           value="<?php echo $optimization['advanced_features']['js_minification']; ?>"
                                        <?php
                                        if (!empty($optimization) &&
                                            $optimization['advanced_features']['jsgroup_minification'] == 1) {
                                            echo 'checked="checked"';
                                        }
                                        ?>>
                                    <div class="slider round"></div>
                                </label>
                            </div>
                        </li>
                        <?php
                        if (class_exists('WpsolAddonSpeedOptimization')) {
                            do_action('wpsol_addon_add_group_google_fonts', $optimization);
                        }
                        ?>

                    </ul>
                    <input type="submit" value="<?php _e('Save settings', 'wp-speed-of-light'); ?>"
                           class="btn waves-effect waves-light" id="advanced-features"/>
                    <br><br>
                    <?php
                    if (class_exists('WpsolAddonSpeedOptimization')) {
                        do_action('wpsol_addon_add_exclude_files', $optimization);
                    } else {
                        ?>
                        <div class="card wpsol_addon_notice light-blue notice notice-success is-dismissible below-h2">
                            <?php echo __('Site file scan, indidual and visual file 
                            exclusion and more options in the optional ', 'wp-speed-of-light'); ?>
                            <a class="pro-addon" href="https://www.joomunited.com/wordpress-products/wp-speed-of-light"
                               target="_blank"><?php echo __('PRO ADDON', 'wp-speed-of-light'); ?></a>
                        </div>
                        <?php
                    }
                    ?>

                </form>
            </div>
        </div>
        <?php
        if (class_exists('WpsolAddonSpeedOptimization')) :
            ?>
            <div id="tab3" class="tab-content cyan lighten-4 active">
                <div class="content-optimization wpsol-optimization">
                    <form class="" method="post" action="">
                        <div class="text-intro">
                            <blockquote>
                                <i><?php _e('This is advanced optimization 
                                featured for advanced users ', 'wp-speed-of-light') ?>
                                    <a href="https://www.joomunited.com/documentation/wp-speed-of-light-documentation"
                                       target="_blank"><?php _e('CHECK DOCUMENTATION >>', 'wp-speed-of-light') ?></a>
                                </i></blockquote>
                        </div>

                        <input type="hidden" name="action" value="wpsol_save_advanced_optimization">
                        <?php wp_nonce_field('wpsol_advanced_optimization', '_wpsol_nonce'); ?>
                        <ul class="field">
                            <?php
                            if (class_exists('WpsolAddonSpeedOptimization')) {
                                do_action('wpsol_addon_add_cache_preloading');
                            }
                            ?>
                        </ul>
                        <input type="submit" value="<?php _e('Save settings', 'wp-speed-of-light'); ?>"
                               class="btn waves-effect waves-light" id="advanced-optimization"/>
                    </form>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php
if (class_exists('WpsolAddonSpeedOptimization')) {
    do_action('wpsol_addon_add_advanced_file_popup');
}
?>


<!--Dialog-->
<div id="wpsol_check_minify_modal" class="check-minify-dialog" style="display: none">
    <div class="check-minify-icon"><i class="material-icons">info_outline</i></div>
    <div class="check-minify-title"><h2><?php _e('File minification activation', 'wp-speed-of-light'); ?></h2></div>
    <div class="check-minify-content">
        <span><?php _e('Check carefully the minification effects 
        on your website, this is a very advanced optimization. If you encounter some errors
         you need to consider disabling it, it has a small impact on performance.', 'wp-speed-of-light'); ?></span>
    </div>
    <div class="check-minify-sucess">
        <input type="button" data-type="" id="agree" class="agree btn waves-effect waves-light "
               value="<?php _e("OK activate it", 'wp-speed-of-light') ?>">
        <input type="button" class="cancel" value="<?php _e('Cancel', 'wp-speed-of-light') ?>">
    </div>
</div>
