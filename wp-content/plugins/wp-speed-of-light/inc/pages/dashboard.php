<?php
if (!class_exists('WpsolDashboard')) {
    require_once(WPSOL_PLUGIN_DIR . '/inc/wpsol-dashboard.php');
}

$dashboard = new WpsolDashboard();
$results = $dashboard->dashboard();
?>

<h1 style="text-align: center;"><?php _e('WP Speed of Light Dashboard', 'wp-speed-of-light') ?></h1>
<div class="dashboard">
    <div class="col-md-9">
        <div class="row panel-statistics">
            <div class="col-sm-6 speedoflight_tool tooltipped" data-position="top"
                 data-tooltip="<?php _e('Check for cache and Gzip activation', 'wp-speed-of-light') ?>">
                <div class="panel panel-updates dashboard-card">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-6 col-lg-7">
                                <h4 class="panel-title dashboard-title">
                                <?php _e('Cache activation', 'wp-speed-of-light') ?></h4>
                                <h3 class="dashboard-title"><?php echo $results['cache'] . '%' ?></h3>
                                <p class="dashboard-title"><?php _e('Optimized at', 'wp-speed-of-light') ?>
                                    : <?php echo $results['cache'] . '%' ?></p>
                                <p class="dashboard-title check_gzip_dashboard">
                                <?php _e('WARNING : GZIP NOT ACTIVATED', 'wp-speed-of-light') ?></p>
                            </div>
                            <div class="col-xs-6 col-lg-5">
                                <div class="progress-rating">
                                    <div class="determinate" style="width: <?php echo $results['cache']; ?>%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 speedoflight_tool tooltipped" data-position="top"
                 data-tooltip="<?php _e('Check for minification HTML, 
                 CSS, JS activation (advanced)', 'wp-speed-of-light') ?>">
                <div class="panel panel-updates dashboard-card">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-6 col-lg-7">
                                <h4 class="panel-title dashboard-title">
                                    <?php _e('Resources minification,HTML,CSS,JS', 'wp-speed-of-light') ?>
                                </h4>
                                <h3 class="dashboard-title"><?php echo $results['minification'] . '%' ?></h3>
                                <p class="dashboard-title"><?php _e('Optimized at', 'wp-speed-of-light') ?>
                                    : <?php echo $results['minification'] . '%' ?></p>
                            </div>
                            <div class="col-xs-6 col-lg-5 ">
                                <div class="progress-rating">
                                    <div class="determinate"
                                         style="width: <?php echo $results['minification']; ?>%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 speedoflight_tool tooltipped" data-position="top"
                 data-tooltip="<?php _e('Check if database cleanup has been made 
                 recently or scheduled in the PRO ADDON', 'wp-speed-of-light') ?>">
                <div class="panel panel-updates dashboard-card">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-6 col-lg-7">
                                <h4 class="panel-title dashboard-title">
                                    <?php _e('Database cleanup', 'wp-speed-of-light') ?>
                                </h4>
                                <h3 class="dashboard-title"><?php echo $results['database-clean'] . '%' ?></h3>
                                <p class="dashboard-title"><?php _e('Optimized at', 'wp-speed-of-light') ?>
                                    : <?php echo $results['database-clean'] . '%' ?></p>
                            </div>
                            <div class="col-xs-6 col-lg-5 ">
                                <div class="progress-rating">
                                    <div class="determinate"
                                         style="width: <?php echo $results['database-clean']; ?>%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 speedoflight_tool tooltipped" data-position="top"
                 data-tooltip="<?php _e('Check for resource group: JS and CSS files +
                  Font for the PRO ADDON', 'wp-speed-of-light') ?>">
                <div class="panel panel-updates dashboard-card">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-6 col-lg-7">
                                <h4 class="panel-title dashboard-title">
                                    <?php _e('GROUP RESOURCES', 'wp-speed-of-light') ?></h4>
                                <h3 class="dashboard-title"><?php echo $results['group_resources'] . '%' ?></h3>
                                <p class="dashboard-title"><?php _e('Optimized at', 'wp-speed-of-light') ?>
                                    : <?php echo $results['group_resources'] . '%' ?></p>
                            </div>
                            <div class="col-xs-6 col-lg-5 ">
                                <div class="progress-rating">
                                    <div class="determinate"
                                         style="width: <?php echo $results['group_resources']; ?>%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 speedoflight_tool tooltipped" data-position="top"
                 data-tooltip="<?php _e('Website loading time by WebPagetest 
                 (latest page analysis results)', 'wp-speed-of-light') ?>">
                <div class="panel panel-updates dashboard-card">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-6 col-lg-7">
                                <h4 class="panel-title dashboard-title">
                                    <?php _e('WEBSITE LOADING TIME', 'wp-speed-of-light') ?></h4>
                                <div class="extension dashboard-title">
                                    <p class="extension-title"><?php _e('Load time', 'wp-speed-of-light') ?>
                                        : <?php echo $results['extension']['load-time'] . ' sec' ?></p>
                                    <p class="extension-title"><?php _e('First bytes', 'wp-speed-of-light') ?>
                                        : <?php echo $results['extension']['first-byte'] . ' sec' ?></p>
                                    <p class="extension-title"><?php _e('Start render', 'wp-speed-of-light') ?>
                                        : <?php echo $results['extension']['render'] . ' sec' ?></p>
                                </div>
                            </div>
                            <div class="col-xs-6 col-lg-5 text-right">
                                <div class="dashboard-icon">
                                    <img src="<?php echo WPSOL_PLUGIN_URL . 'css/images/loading-time-dash.png'; ?>"
                                         alt="loading time dashboard"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-sm-6 speedoflight_tool tooltipped" data-position="top"
                 data-tooltip="<?php _e('Database queries analysis (latest analysis)', 'wp-speed-of-light') ?>">
                <div class="panel panel-updates dashboard-card">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-6 col-lg-7">
                                <h4 class="panel-title dashboard-title">
                                    <?php _e('DATABASE QUERIES', 'wp-speed-of-light') ?></h4>
                                <div class="extension dashboard-title">
                                    <p class="extension-title"><?php _e('Plugins', 'wp-speed-of-light') ?>
                                        : <?php echo $results['queries']['plugin'] . ' sec' ?></p>
                                    <p class="extension-title"><?php _e('Themes', 'wp-speed-of-light') ?>
                                        : <?php echo $results['queries']['themes'] . ' sec' ?></p>
                                    <p class="extension-title"><?php _e('WP Core', 'wp-speed-of-light') ?>
                                        : <?php echo $results['queries']['wp-core'] . ' sec' ?></p>
                                </div>
                            </div>
                            <div class="col-xs-6 col-lg-5 text-right">
                                <div class="dashboard-icon">
                                    <img src="<?php echo WPSOL_PLUGIN_URL . 'css/images/database-dash.png'; ?>"
                                         alt="database dashboard"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 speedoflight_tool tooltipped" data-position="top"
                 data-tooltip="<?php _e('PHP version check', 'wp-speed-of-light') ?>">
                <div class="panel panel-updates dashboard-card">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-8 col-lg-9">
                                <h4 class="panel-title dashboard-title">
                                    <?php _e('PHP VERSION CHECK', 'wp-speed-of-light') ?></h4>
                                <div class="notice-analysis dashboard-title">
                                    <?php if (version_compare(PHP_VERSION, '7.0.0', '<')) : ?>
                                        <p><?php _e('Your PHP version is: 5.6.x It would be great 
                                        to install or activate PHP 7+.Comparing to previous versions 
                                        the execution time is more than twice as fast
                                         and has 30 percent lower memory consumption.', 'wp-speed-of-light') ?></p>
                                        <?php
                                    elseif (version_compare(PHP_VERSION, '7.1.0', '>')) :?>
                                        <p><?php _e("OK! Your PHP version is: 7.1.x. That's great, 
                                        because comparing to previous 5.6 versions the execution 
                                        time of PHP 7.1 is more than twice as fast and has 
                                        30 percent lower memory consumption.", 'wp-speed-of-light') ?></p>
                                    <?php else : ?>
                                        <p><?php _e("OK! Your PHP version is: 7.0.x. That's great, 
                                        because comparing to previous 5.6 versions the execution 
                                        time of PHP 7.0 is more than twice as fast and has 
                                        30 percent lower memory consumption.", 'wp-speed-of-light') ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-xs-4 col-lg-3 text-right">
                                <div class="dashboard-icon">
                                    <img src="<?php echo WPSOL_PLUGIN_URL . 'css/images/php-build-check.png'; ?>"
                                         alt="PHP version check dashboard"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 speedoflight_tool tooltipped" data-position="top"
                 data-tooltip="<?php _e('Other recommendations', 'wp-speed-of-light') ?>">
                <div class="panel panel-updates dashboard-card">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-8 col-lg-9">
                                <h4 class="panel-title dashboard-title">
                                    <?php _e('Other recommendations', 'wp-speed-of-light') ?></h4>
                                <div class="notice-analysis dashboard-title">
                                    <?php if ($results['plugins_enable'] >= 20) : ?>
                                        <p><?php _e('&bull;&nbsp;&nbsp;&nbsp;&nbsp;You have more than 20 plugins 
                                        installed and activated, the less you have better 
                                        it is for your loading time', 'wp-speed-of-light') ?></p>
                                        <?php
                                    endif;
                                    ?>
                                    <?php
                                    if ($results['plugins_disable'] >= 5) :
                                        ?>
                                        <p><?php _e('&bull;&nbsp;&nbsp;&nbsp;&nbsp;You have more than 
                                        5 plugins disabled, you may consider removing them 
                                        if they’re not useful', 'wp-speed-of-light') ?></p>
                                    <?php endif; ?>
                                    <p class="check_gzip_dashboard">
                                        <?php _e('&bull;&nbsp;&nbsp;&nbsp;&nbsp;GZIP activation: 
                                        In order to activate GZIP it has to be enabled 
                                        on the server (ask your hosting provider)
                                        , and the necessary rules placed in your .htaccess file,
                                        located in the root directory of your site', 'wp-speed-of-light') ?></p>
                                </div>
                            </div>
                            <div class="col-xs-4 col-lg-3 text-right">
                                <div class="dashboard-icon">
                                    <img src="<?php echo WPSOL_PLUGIN_URL . 'css/images/recommendations-icon.png'; ?>"
                                         alt="Other recommendations dashboard"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>

</div>