<?php
    $list_items = array(
        'Cache preloading',
        'Local and Google font optimization',
        'Visual file Inclusion/Exclusion',
        'File cache exclusion rules',
        'DNS Prefetching',
        'Database automatic cleanup',
        'Disable cache by user role',
        'CDN cache clean'
    );
?>
<div class="more-speedup">
    <h1><?php _e('WP Speed of Light Pro Addon', 'wp-speed-of-light') ?></h1>
    <label class="title">
        <a class="pro-addon" href="https://www.joomunited.com/wordpress-products/wp-speed-of-light" target="_blank">
            <?php _e('SpeedUp all your WordPress websites with a single PRO ADDON', 'wp-speed-of-light') ?>
        </a>
    </label>
    <div class="list-item">
        <ul>
            <?php foreach ($list_items as $item) : ?>
             <li><?php echo $item; ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>