<?php
if (isset($_POST['clean'])) {
    update_option('wpsol_clean_database_config', $_POST['clean']);
    foreach ($_POST['clean'] as $type) {
        $mess = WpsolDatabaseCleanup::cleanSystem($type);
    }
}
$cleanup = new WpsolDatabaseCleanup;
?>
<h1 class="wpsol-clean-title"><label><?php _e('WordPress database cleanup', 'wp-speed-of-light') ?></label></h1>
<?php
if (!empty($mess) && $mess !== '') {
    echo '<div id="message" class="notice notice-success">';
    echo '<strong>' . $mess . '</strong></div>';
}

?>
<div id="wpsol-clean-tab">
    <form method="POST">
        <?php wp_nonce_field('wpsol_database_clean', '_wpsol_nonce'); ?>
        <ul class="wpsol-list-clean">
            <input type="hidden" name="database_clean_settings" value="1"/>
            <?php

            $check = array('', '', '', '', '', '', '', '', '', '', '');
            if (class_exists('WpsolAddonDatabaseCleanup')) {
                do_action('wpsol_addon_add_auto_cleanup_db_by_interval');

                $check = apply_filters('wpsol_addon_check_input_db_cleanup', $check);
            }
            ?>
            <li>
                <input type="checkbox" id="data0" name="all_control" value="all_data"/>
                <label for="data0" class="speedoflight_tool text-list"
                       alt="<?php _e('Select all database ', 'wp-speed-of-light') ?>">
                    <?php _e('Select all', 'wp-speed-of-light') ?></label>
            </li>
            <li>
                <input type="checkbox" id="data1" name="clean[]" <?php echo $check[0]; ?> class="clean-data"
                       value="revisions"/>
                <label for="data1" class="speedoflight_tool text-list"
                       alt="<?php _e('WordPress, by default, is generating content revisions (copy) 
                       to restore it from an old version. You don’t need revisions? cleanup!', 'wp-speed-of-light') ?>">
                    <?php _e('Clean all post revisions', 'wp-speed-of-light') ?>

                    <?php echo "&nbsp(" . $cleanup->getElementToClean('revisions') . ")"; ?></label>
            </li>
            <li>
                <input type="checkbox" id="data2" name="clean[]" <?php echo $check[1]; ?> class="clean-data"
                       value="drafted"/>
                <label for="data2" class="speedoflight_tool text-list"
                       alt="<?php _e('WordPress, by default, have an auto saving feature 
                       to restore content from the latest auto saved version. 
                       You don’t need auto saved content? cleanup!', 'wp-speed-of-light') ?>">
                    <?php _e('Clean all auto drafted content', 'wp-speed-of-light') ?>

                    <?php echo "&nbsp(" . $cleanup->getElementToClean('drafted') . ")"; ?></label>
            </li>
            <li>
                <input type="checkbox" id="data3" name="clean[]" <?php echo $check[2]; ?> class="clean-data"
                       value="trash"/>
                <label for="data3" class="speedoflight_tool text-list"
                       alt="<?php _e('All content (post, page…) in trash will be cleaned up', 'wp-speed-of-light') ?>">
                    <?php _e('Remove all trashed content', 'wp-speed-of-light') ?>

                    <?php echo "&nbsp(" . $cleanup->getElementToClean('trash') . ")"; ?></label>
            </li>
            <li>
                <input type="checkbox" id="data4" name="clean[]" <?php echo $check[3]; ?> class="clean-data"
                       value="comments"/>
                <label for="data4" class="speedoflight_tool text-list"
                       alt="<?php _e('All comments in trash or classified 
                       as spam will be cleaned up', 'wp-speed-of-light') ?>">
                    <?php _e('Remove comments from trash & spam', 'wp-speed-of-light') ?>

                    <?php echo "&nbsp(" . $cleanup->getElementToClean('comments') . ")"; ?></label>
            </li>
            <li>
                <input type="checkbox" id="data5" name="clean[]" <?php echo $check[4]; ?> class="clean-data"
                       value="trackbacks"/>
                <label for="data5" class="speedoflight_tool text-list"
                       alt="<?php _e('Trackbacks and pingbacks are methods for alerting blogs
                        that you have linked to them. You don’t need revisions? Cleanup!', 'wp-speed-of-light') ?>">
                    <?php _e('Remove trackbacks and pingbacks', 'wp-speed-of-light') ?>

                    <?php echo "&nbsp(" . $cleanup->getElementToClean('trackbacks') . ")"; ?></label>
            </li>
            <li>
                <input type="checkbox" id="data6" name="clean[]" <?php echo $check[5]; ?> class="clean-data"
                       value="transient"/>
                <label for="data6" class="speedoflight_tool text-list"
                       alt="<?php _e('Transient options is something like a basic cache system used by wordpress. 
                       No risk it’s regenerated by WordPress automatically', 'wp-speed-of-light') ?>">
                    <?php _e('Remove transient options', 'wp-speed-of-light') ?>

                    <?php echo "&nbsp(" . $cleanup->getElementToClean('transient') . ")"; ?></label>
            </li>
            <?php
            if (class_exists('WpsolAddonDatabaseCleanup')) {
                do_action('wpsol_addon_add_stuff_database_cleanup', $cleanup, $check);
            }
            ?>
        </ul>
        <input type="submit" value="<?php _e('Clean and Save', 'wp-speed-of-light') ?>"
               class="btn waves-effect waves-light"/>
    </form>
    <?php
    if (!class_exists('WpsolAddonDatabaseCleanup')) :
        ?>
        <div class="card wpsol_addon_notice light-blue notice notice-success is-dismissible below-h2"
             style="max-width: 550px">
            <?php echo __('Run automatic database cleanup with more options in the optional ', 'wp-speed-of-light'); ?>
            <a class="pro-addon" href="https://www.joomunited.com/wordpress-products/wp-speed-of-light"
               target="_blank"><?php echo __('PRO ADDON', 'wp-speed-of-light'); ?></a>
        </div>
        <?php
    endif;
    ?>
</div>
    
