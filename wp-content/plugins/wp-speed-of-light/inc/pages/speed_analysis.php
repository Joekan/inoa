<?php
if (!class_exists('WpsolSpeedAnalysis')) {
    require_once(WPSOL_PLUGIN_DIR . '/inc/wpsol-speed-analysis.php');
}

$wpsol_SpeedAnalysis = new WpsolSpeedAnalysis;
$queriesParameter = $wpsol_SpeedAnalysis->getInfoQueries();
$lastest = get_option('wpsol_loadtime_lastest');
$element = get_option('wpsol_loadpage_element');
$loadtime_result = get_option('wpsol_loadtime_result');
$conf = get_option('wpsol_configuration');
if (!empty($loadtime_result)) {
    $loadtime_result = array_reverse($loadtime_result);
}

// total result queries
if (!empty($queriesParameter)) {
    $plugin_time = 0;
    $select = $wpsol_SpeedAnalysis->getTotalResultQueries($queriesParameter, 'SELECT');
    $show = $wpsol_SpeedAnalysis->getTotalResultQueries($queriesParameter, 'SHOW');
    $update = $wpsol_SpeedAnalysis->getTotalResultQueries($queriesParameter, 'UPDATE');
    foreach ($queriesParameter['plugin']['details'] as $k => $v) {
        $plugin_time += $v['load_time'];
    }
    $time = $queriesParameter['theme']['load_time'] + $queriesParameter['core']['load_time'] + $plugin_time;
}
$active_plugins = 0;
// count total plugin
$active_plugins = count(get_mu_plugins());
foreach (get_plugins() as $plugin => $junk) {
    if (is_plugin_active($plugin)) {
        $active_plugins++;
    }
}

?>
<div id="tabs-analysis">
    <ul class="tabs z-depth-1 cyan">
        <li class="tab " id="tabAnalysis"><a data-tab-id="tabAnalysis" id="tab-tabAnalysis"
                                             class="link-tab white-text waves-effect waves-light"
                                             href="#tab1"><?php _e('Loading time', 'wp-speed-of-light') ?></a></li>
        <li class="tab" id="tabQuery"><a data-tab-id="tabQuery" id="tab-tabQuery"
                                         class="link-tab white-text waves-effect waves-light"
                                         href="#tab2"><?php _e('Database queries', 'wp-speed-of-light') ?></a></a></li>
    </ul>

    <div class="tabs-content">
        <div id="tab1" class="tab-content cyan lighten-4">
            <!--analysis-->
            <div class="launch-analysis">
                <label for="insert-url"
                       class="insert-url"><?php _e('URL to analyse : ', 'wp-speed-of-light') ?>
                    <?php echo home_url() . '/'; ?></label>
                <input id="insert-url" type="text" name="wpsol_url_speed" value="" class="wpsol_url_speed"/>
                <input id="old-url" type="text" readonly="true"
                       data-url="<?php echo (!empty($element)) ? $element['url'] : ''; ?>" style="display: none;"/>
                <input id="main-url" type="text" readonly="true" data-url="<?php echo home_url(); ?>"
                       style="display: none;"/>
                <input id="speed-button" type="button" value="<?php _e('Loading time test', 'wp-speed-of-light') ?>"
                       name="loadtime-button" class="btn waves-effect waves-light"/>
            </div>
            <div id="message-scan" style="display: none; margin-top:20px;" class="notice notice-success">
                <strong><?php _e('The speed test is running… it may takes between 1 and 5 minutes,
                 keep calm and stay cool :)', 'wp-speed-of-light'); ?>
                </strong>
            </div>
            <!--progess-->
            <div class="scan-test-progress progress" style="display:none">
                <div class="indeterminate"></div>
            </div>
            <?php if (!isset($conf['webtest_api_key']) ||
                (isset($conf['webtest_api_key']) &&
                    $conf['webtest_api_key'] == '')) : ?>
                <div id="message-not-key" style="" class=" notice notice-success">
                    <?php _e('To run speed test you need to add into the configuration
                     a free WebPagetest API key ', 'wp-speed-of-light') ?>
                    <a id="register-key" class="btn waves-effect waves-light waves-input-wrapper"
                       href="https://www.webpagetest.org/getkey.php"
                       target="_blank"><?php _e('GET IT NOW', 'wp-speed-of-light') ?></a> <br><br>
                    <?php _e('Run free website speed test using real browsers and at real consumer connection speeds.
                     Your results will provide diagnostic information including resources,
                      loading time and optimization.', 'wp-speed-of-light') ?>
                </div>
            <?php endif; ?>
            <div id="message-error-scan" style="display: none; padding: 10px;" class="notice notice-warning">
                <strong><?php _e('An error occurred during the scan . Please check again<br>
                Please note that scan can’t work for local environments', 'wp-speed-of-light'); ?></strong>
            </div>
            <div class="tab-testpage-content">
                <div class="box-result">
                    <ul>
                        <li>
                            <h4><?php _e('Total plugins ', 'wp-speed-of-light') ?>
                                :&nbsp;<?php echo $active_plugins; ?></h4>
                            <p><?php _e('The number of plugins currently activated', 'wp-speed-of-light') ?></p>
                        </li>
                        <li>
                            <h4><?php _e('Loading time', 'wp-speed-of-light') ?></h4>
                            <p><?php _e('Lastest speed analysis test result: ', 'wp-speed-of-light') ?>
                                &nbsp;<?php echo $lastest['average-loading']; ?>
                                &nbsp;<?php _e('sec', 'wp-speed-of-light'); ?></p>
                        </li>
                    </ul>
                </div>
                <div class="lastest-details">
                    <h3 class="loadtime-title"><?php _e('Lastest analysis details', 'wp-speed-of-light') ?></h3>
                    <table width="100%" class="lastest-details-table" style="border-collapse: collapse;">
                        <tr>
                            <th></th>
                            <th class="tooltipped" data-position="bottom"
                                data-tooltip="<?php _e('Load time Tooltip', 'wp-speed-of-light') ?>">
                                <?php _e('Load time', 'wp-speed-of-light') ?></th>
                            <th class="tooltipped" data-position="bottom"
                                data-tooltip="<?php _e('First byte Tooltip', 'wp-speed-of-light') ?>">
                                <?php _e('First byte', 'wp-speed-of-light') ?></th>
                            <th class="tooltipped" data-position="bottom"
                                data-tooltip="<?php _e('Time start render Tooltip', 'wp-speed-of-light') ?>">
                                <?php _e('Start render', 'wp-speed-of-light') ?></th>
                            <th class="tooltipped" data-position="bottom"
                                data-tooltip="<?php _e('Score caching Tooltip', 'wp-speed-of-light') ?>"
                                style='text-align: left;padding-left: 45px;'>
                                <?php _e('Caching', 'wp-speed-of-light') ?></th>
                            <th class="tooltipped" data-position="bottom"
                                data-tooltip="<?php _e('Score gzip Tooltip', 'wp-speed-of-light') ?>"
                                style='text-align: left;padding-left: 55px;'>
                                <?php _e('Gzip', 'wp-speed-of-light') ?></th>
                            <th class="tooltipped" data-position="bottom"
                                data-tooltip="<?php _e('Score image compresssion Tooltip', 'wp-speed-of-light') ?>"
                                style='text-align: left;padding-left: 15px;'>
                                <?php _e('Image compression', 'wp-speed-of-light') ?></th>
                        </tr>
                        <tr class="tooltipped" data-position="top"
                            data-tooltip="<?php _e('This is first time when
                             page loaded Tooltip', 'wp-speed-of-light') ?>">
                            <td style="border-bottom: 1px solid #EEEEEE;">
                                <?php _e('First load', 'wp-speed-of-light') ?></td>
                            <td><?php echo $lastest['first']['load-time']; ?></td>
                            <td><?php echo $lastest['first']['first-byte']; ?></td>
                            <td><?php echo $lastest['first']['render']; ?></td>
<td><?php $wpsol_SpeedAnalysis->starRating($lastest['first']['caching'], 'first-caching'); ?></td>
<td><?php $wpsol_SpeedAnalysis->starRating($lastest['first']['gzip'], 'first-gzip'); ?></td>
<td><?php $wpsol_SpeedAnalysis->starRating($lastest['first']['compression'], 'first-compression'); ?></td>
                        </tr>
                        <tr class="tooltipped" data-position="top"
                            data-tooltip="<?php _e('This is second time when
                             page loaded Tooltip', 'wp-speed-of-light') ?>">
                            <td style="border-bottom: 1px solid #EEEEEE;">
                                <?php _e('Second load', 'wp-speed-of-light') ?></td>
                            <td><?php echo $lastest['second']['load-time']; ?></td>
                            <td><?php echo $lastest['second']['first-byte']; ?></td>
                            <td><?php echo $lastest['second']['render']; ?></td>
<td><?php $wpsol_SpeedAnalysis->starRating($lastest['second']['caching'], 'second-caching'); ?></td>
<td><?php $wpsol_SpeedAnalysis->starRating($lastest['second']['gzip'], 'second-gzip'); ?></td>
<td><?php $wpsol_SpeedAnalysis->starRating($lastest['second']['compression'], 'second-compression'); ?></td>
                        </tr>
                    </table>
                </div>
                <div class="lastest-speed-test">
                    <h3 class="loadtime-title"><?php _e('10 lastest speed tests', 'wp-speed-of-light') ?></h3>
                    <table width="100%" class="lastest-details-table" id="ten-details"
                           style="border-collapse: collapse;">
                        <tr>
                            <th class="tooltipped" data-position="bottom"
                                data-tooltip="<?php _e('Thumbnail Tooltip', 'wp-speed-of-light') ?>">
                                <?php _e('Thumbnail', 'wp-speed-of-light') ?></th>
                            <th class="tooltipped" data-position="bottom"
                                data-tooltip="<?php _e('Url Tooltip', 'wp-speed-of-light') ?>">
                                <?php _e('URL', 'wp-speed-of-light') ?></th>
                            <th class="tooltipped" data-position="bottom"
                                data-tooltip="<?php _e('Load time Tooltip', 'wp-speed-of-light') ?>">
                                <?php _e('Load time', 'wp-speed-of-light') ?></th>
                            <th class="tooltipped" data-position="bottom"
                                data-tooltip="<?php _e('Details Tooltip', 'wp-speed-of-light') ?>"></th>
                        </tr>
                        <?php if (!empty($loadtime_result)) : ?>
                            <?php foreach ($loadtime_result as $v) : ?>
                                <tr id="<?php echo md5($v['url']); ?>">
                                    <td style="text-align: center;width:25%"><img src="<?php echo $v['thumbnail'] ?>"
                                    style="border: 1px double #000;width:100%;max-width:150px;"/>
                                    </td>
                                    <td style="text-align: left;width:25%"><a href="<?php echo $v['url'] ?>"
                                     style="text-decoration: none;"
                                     target="_blank"><?php echo $v['url'] ?></a>
                                    </td>
                                    <td style="text-align: center;width:25%"><?php echo $v['load-time'] ?></td>
                                    <td style="width:15% ;"><input type="button"
                                         value="<?php _e('More details', 'wp-speed-of-light') ?>"
                                        data-url="<?php echo $v['url']; ?>"
                                        class="wpsol-more-details btn waves-effect waves-light"/>
                                    </td>
                                    <td style="width:10% ;"><i class="mdi-content-clear clear-test tooltipped"
                                    data-position="top"
                                    data-tooltip="<?php _e('Remove speed test Tooltip', 'wp-speed-of-light') ?>"
                                    data-url="<?php echo $v['url']; ?>"></i></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </table>

                </div>
            </div>
        </div>


        <div id="tab2" class="tab-content cyan lighten-4">
            <div class="launch-analysis">
                <label for="insert-url-queries"
                       class="insert-url"><?php _e('URL to analyse : ', 'wp-speed-of-light') ?>
                    <?php echo home_url() . '/'; ?></label>
                <input type="hidden" value="<?php echo home_url(); ?>" id="main-url-queries"/>
                <input id="insert-url-queries" type="text" name="wpsol_url_queries" value="" class="wpsol_url_queries"/>
                <input id="query-button" type="button" value="<?php _e('Launch analysis', 'wp-speed-of-light') ?>"
                       class="btn waves-effect waves-light"/>
            </div>
            <!--box result-->
            <div class="box-result">
                <ul>
                    <li>
                        <h4><?php _e('Total plugins ', 'wp-speed-of-light') ?>
                            :&nbsp;<?php echo $queriesParameter['plugin']['total_plugin']; ?>&nbsp;</h4>
                        <p><?php _e('Queries time =', 'wp-speed-of-light') ?>
                            &nbsp;<?php echo ($plugin_time) ? $plugin_time : 0; ?>
                            &nbsp;<?php _e('sec', 'wp-speed-of-light'); ?></p>
                    </li>
                    <li>
                        <h4><?php _e('Theme ', 'wp-speed-of-light') ?></h4>
                        <p><?php _e('Loading time =', 'wp-speed-of-light') ?>
                            &nbsp;<?php echo $queriesParameter['theme']['load_time']; ?>
                            &nbsp;<?php _e('sec', 'wp-speed-of-light'); ?></p>
                    </li>
                    <li>
                        <h4><?php _e('WP Core ', 'wp-speed-of-light') ?></h4>
                        <p><?php _e('Queries time =', 'wp-speed-of-light') ?>
                            &nbsp;<?php echo $queriesParameter['core']['load_time']; ?>
                            &nbsp;<?php _e('sec', 'wp-speed-of-light'); ?></p>
                    </li>
                </ul>
            </div>
            <!--chart plugins-->

            <!--            <div class="chart-plugins">
                            <div id="chartContainerQuery" class="chartContainer" style="height: 250px; width: 60%;" >
                            </div>
                        </div>-->


            <!--table plugins-->
            <div class="table-queries">
                <table id="table-sorter-queries" class="tablesorter" align="center">
                    <thead>
                    <tr>
                        <td style="text-align: left; border: 1px #dad9c7 solid;  color: #000; padding-left: 5px;"
                            colspan="5"><?php _e('Query by component', 'wp-speed-of-light'); ?></td>
                    </tr>
                    <tr>
                        <th class="top-header"
                            style="color: #000;"><?php _e('WordPress & Plugins', 'wp-speed-of-light'); ?></th>
                        <th><?php _e('Select', 'wp-speed-of-light'); ?></th>
                        <th><?php _e('Show', 'wp-speed-of-light'); ?></th>
                        <th><?php _e('Update', 'wp-speed-of-light'); ?></th>
                        <th><?php _e('Time', 'wp-speed-of-light'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($queriesParameter as $k => $v) :
                        if ($k == 'theme' || $k == 'core') :
                            ?>
                            <tr>
                                <td class="top-header" style="text-transform: capitalize"><?php echo $k; ?></td>
                                <td><?php
                                if (isset($v['type']['SELECT'])) {
                                    echo $v['type']['SELECT'];
                                }
                                    ?></td>
                                <td>
                                    <?php
                                    if (isset($v['type']['SHOW'])) {
                                        echo $v['type']['SHOW'];
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (isset($v['type']['UPDATE'])) {
                                        echo $v['type']['UPDATE'];
                                    }
                                    ?>
                                </td>
                                <td><?php echo $v['load_time']; ?></td>
                            </tr>

                            <?php
                        endif;
                        if ($k == 'plugin') :
                            foreach ($v['details'] as $key => $value) :
                                ?>
                                <tr>
                                    <td class="top-header"><?php echo 'Plugin: ' . $key; ?></td>
                                    <td><?php
                                    if (isset($value['type']['SELECT'])) {
                                        echo $value['type']['SELECT'];
                                    } else {
                                        echo 0;
                                    }
                                        ?></td>
                                    <td>
                                        <?php
                                        if (isset($value['type']['SHOW'])) {
                                            echo $value['type']['SHOW'];
                                        } else {
                                            echo 0;
                                        }

                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        if (isset($value['type']['UPDATE'])) {
                                            echo $value['type']['UPDATE'];
                                        } else {
                                            echo 0;
                                        }
                                        ?>
                                    </td>
                                    <td><?php echo $value['load_time']; ?></td>
                                </tr>
                                <?php
                            endforeach;
                        endif;
                    endforeach;
                    ?>

                    </tbody>
                    <tr>
                        <td></td>
                        <td><?php echo ($select) ? $select : 0; ?></td>
                        <td><?php echo ($show) ? $show : 0; ?></td>
                        <td><?php echo ($update) ? $update : 0; ?></td>
                        <td><?php echo ($time) ? $time : 0; ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    jQuery(document).ready(function ($) {
        var methods = {
            init: function () {
                return this.each(function () {

                    // For each set of tabs, we want to keep track of
                    // which tab is active and its associated content
                    var $this = $(this),
                        window_width = $(window).width();

                    $this.width('100%');
                    var $active, $content, $links = $this.find('li.tab a'),
                        $tabs_width = $this.width(),
                        $tab_width = $this.find('li').first().outerWidth(),
                        $index = 0;

                    // If the location.hash matches one of the links, use that as the active tab.
                    $active = $($links.filter('[href="' + location.hash + '"]'));

                    // If no match is found, use the first link or any with class 'active' as the initial active tab.
                    if ($active.length === 0) {
                        $active = $(this).find('li.tab a.active').first();
                    }
                    if ($active.length === 0) {
                        $active = $(this).find('li.tab a').first();
                    }

                    $active.addClass('active');
                    $index = $links.index($active);
                    if ($index < 0) {
                        $index = 0;
                    }

                    $content = $($active[0].hash);

                    // append indicator then set indicator width to tab width
                    $this.append('<div class="indicator"></div>');
                    var $indicator = $this.find('.indicator');
                    if ($this.is(":visible")) {
                        $indicator.css({"right": $tabs_width - (($index + 1) * $tab_width)});
                        $indicator.css({"left": $index * $tab_width});
                    }
                    $(window).resize(function () {
                        $tabs_width = $this.width();
                        $tab_width = $this.find('li').first().outerWidth();
                        if ($index < 0) {
                            $index = 0;
                        }
                        if ($tab_width !== 0 && $tabs_width !== 0) {
                            $indicator.css({"right": $tabs_width - (($index + 1) * $tab_width)});
                            $indicator.css({"left": $index * $tab_width});
                        }
                    });

                    // Hide the remaining content
                    $links.not($active).each(function () {
                        $(this.hash).hide();
                    });


                    // Bind the click event handler
                    $this.on('click', 'a', function (e) {
                        if ($(this).parent().hasClass('disabled')) {
                            e.preventDefault();
                            return;
                        }

                        $tabs_width = $this.width();
                        $tab_width = $this.find('li').first().outerWidth();

                        // Make the old tab inactive.
                        $active.removeClass('active');
                        $content.hide();

                        // Update the variables with the new link and content
                        $active = $(this);
                        $content = $(this.hash);
                        $links = $this.find('li.tab a');

                        // Make the tab active.
                        $active.addClass('active');
                        var $prev_index = $index;
                        $index = $links.index($(this));
                        if ($index < 0) {
                            $index = 0;
                        }
                        // Change url to current tab
                        // window.location.hash = $active.attr('href');

                        $content.show();

                        // Update indicator
                        if (($index - $prev_index) >= 0) {
                            $indicator.velocity({"right": $tabs_width - (($index + 1) * $tab_width)}, {
                                duration: 300,
                                queue: false,
                                easing: 'easeOutQuad'
                            });
                            $indicator.velocity({"left": $index * $tab_width}, {
                                duration: 300,
                                queue: false,
                                easing: 'easeOutQuad',
                                delay: 90
                            });

                        } else {
                            $indicator.velocity({"left": $index * $tab_width}, {
                                duration: 300,
                                queue: false,
                                easing: 'easeOutQuad'
                            });
                            $indicator.velocity({"right": $tabs_width - (($index + 1) * $tab_width)}, {
                                duration: 300,
                                queue: false,
                                easing: 'easeOutQuad',
                                delay: 90
                            });
                        }

                        var chartQuery = new CanvasJS.Chart("chartContainerQuery",
                            {
                                animationEnabled: true,
                                data: [
                                    {
                                        type: "bar",
                                        color: "blue",
                                        dataPoints: [
                                            <?php foreach ($queriesParameter['plugin']['details'] as $k => $v) { ?>
                                            {y: <?php echo $v['load_time']; ?>, label: "<?php echo $k; ?>"},
                                            <?php } ?>
                                        ]
                                    }
                                ]
                            });
                        //render chart
//                        chartQuery.render();
                        jQuery("#table-sorter-queries").tablesorter({sortList: [[0, 0], [1, 0]]});

                        // Prevent the anchor's default click action
                        e.preventDefault();
                    });
                });

            },
            select_tab: function (id) {
                this.find('a[href="#' + id + '"]').trigger('click');
            }
        };

        $.fn.tabs = function (methodOrOptions) {
            if (methods[methodOrOptions]) {
                return methods[methodOrOptions].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
                // Default to "init"
                return methods.init.apply(this, arguments);
            } else {
                $.error('Method ' + methodOrOptions + ' does not exist on jQuery.tooltip');
            }
        };

        $('ul.tabs').tabs();

        $(".link-tab").click(function (e) {
            var id_tab = $(this).data('tab-id');
            document.cookie = 'wpsol_active_tab=' + id_tab;
        });

        function setTabFromCookie() {
            var active_tab = getCookie('wpsol_active_tab');
            if (active_tab !== "") {
                $("#tab-" + active_tab).click();
            }
        }

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ')
                    c = c.substring(1);
                if (c.indexOf(name) == 0)
                    return c.substring(name.length, c.length);
            }
            return "";
        }

        setTabFromCookie();

    });

</script>


<!-- Dialog for iframe scanner -->
<div id="wpsol-scanner-dialog" class="wpsol-dialog">
    <iframe id="wpsol-scan-frame" frameborder="0"
            data-defaultsrc="">
    </iframe>
    <div id="wpsol-scan-caption">
        <?php _e('The scanner will analyze the speed and resource usage of all active plugins on your website.
         It may take several minutes, and this window must
         remain open for the scan to finish successfully.', 'wp-speed-of-light'); ?>
    </div>
</div>

<!-- Dialog for progress bar -->
<div id="wpsol-progress-dialog" class="wpsol-dialog">
    <div id="wpsol-scanning-caption">
        <?php _e('Scanning ...', 'wp-speed-of-light'); ?>
    </div>
    <div id="wpsol-progress"></div>

    <!-- View results button -->
    <div class="wpsol-big-button" id="wpsol-view-results-buttonset" style="display: none;">
        <input type="checkbox" id="wpsol-view-results-submit" class="view-results-button" checked="checked"
               data-scan-name=""/>
        <label for="wpsol-view-results-submit"
               class="btn waves-effect waves-light"><?php _e('View Results', 'wp-speed-of-light'); ?></label>
    </div>
</div>

<!-- Dialog for more details -->

<div id="wpsol-more-details-dialog" class="wpsol-modal" style="display: none;">
    <span class="wpsol-close">×</span>
    <div class="wpsol-modal-content">
    </div>
</div>