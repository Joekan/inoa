jQuery(document).ready(function ($) {
    // check gzip

    $.ajax({
        url : ajaxurl,
        method : "POST",
        dataType : "json",
        data:{
            action : 'wpsol_check_gzip_activated'
        },success : function(res){
            if (!res){
                $('.dashboard p.check_gzip_dashboard').show();
            }
        }
    });
});