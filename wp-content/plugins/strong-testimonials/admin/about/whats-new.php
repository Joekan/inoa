<h2>Now Even Stronger</h2>

<p class="lead-description">Version 2.30 adds highly requested features and fixes one bug.</p>
<div class="feature-section one-col">
    <div class="col">
        <p>NEW: Option to select the font color in views. Finally!</p>
        <p>NEW: Option to add a CSS class to the image for lightboxes.</p>
        <p>NEW: Certain shortcode attributes will override view settings. This allows views to serve as reusable patterns.</p>
        <p>NEW: Using the WordPress email validation function for increased security.</p>
        <p>NEW: Using the WordPress number format function in the &#91;testimonial_count&#93; shortcode so the thousands separator will be properly localized.</p>
        <p>IMPROVED: Templates were completely reorganized to consolidate options and prepare for future features.</p>
        <p>IMPROVED: Simplified slideshow CSS will make adding custom CSS simpler.</p>
        <p>NEW: Option to disable touch swipe navigation to improve slideshow behavior on mobile devices.</p>
        <p>FIXED: An intermittent bug in the slideshow in Chrome.</p>
    </div>
</div>

<p class="lead-description">Version 2.29 improves Captcha options and fixes two bugs.</p>
<div class="feature-section one-col">
    <div class="col">
        <p>NEW: Integration with Google Captcha by BestWebSoft. This allows use of either Invisible reCaptcha and reCatpcha V2, both excellent choices for modern spam warfare.</p>
        <p>REMOVED: Integration with the plugin Captcha due to security issues.</p>
        <p>REMOVED: Integration with the plugin Advanced noCaptcha reCaptcha due to compatibility issues and inadequate maintenance.</p>
        <p>FIXED: The notification email now displays a category name properly.</p>
        <p>IMPROVED: The notification settings UI was improved to make it easier to add your custom fields to the notification email.</p>
        <p>NEW: A new filter on the "Read more" page for total control over that link.</p>
        <p>FIXED: A conflict with Cherry Slider that made the slideshow invisible.</p>
    </div>
</div>

<p class="lead-description">Version 2.28 improves compatibility and adds some new features.</p>
<div class="feature-section one-col">
    <div class="col">
        <p>NEW: Compatibility settings for sites that use Ajax page loading via theme or plugin.</p>
        <p>NEW: Views using simple pagination now provide a subtle transition effect.</p>
        <p>NEW: Standard pagination options including Previous/Next links and condensed navigation.</p>
        <p>IMPROVED: A JavaScript controller to coordinate multiple components like slideshows and paginated views.</p>
        <p>IMPROVED: The slider and pager scripts were refactored to improve performance. <strong style="color: #8224E3;">Clear your caches!</strong></p>
        <p>FIXED: Bug where standard pagination failed to include a trailing slash.</p>
        <p>FIXED: Capabilities check in multisite.</p>
        <p>IMPROVED: Many functions were refactored into classes to improve performance and future development.</p>
    </div>
</div>

<p class="lead-description">Version 2.27 adds some new features, fixes a few bugs and improves compatibility.</p>
<div class="feature-section one-col">
    <div class="col">
        <p>FIXED: Optional loading of Font Awesome.</p>
        <p>FIXED: Width on double-digit pagination controls in Modern template.</p>
        <p>FIXED: Hide slider controls if no slides are present.</p>
        <p>FIXED: CSS for slider controls in Internet Explorer 11.</p>
        <p>NEW: Unique user capabilities for workflow control.</p>
        <p>NEW: Option to set checkbox field on by default.</p>
        <p>NEW: Option to exclude from Lazy Loading Responsive Images plugin (on by default).</p>
        <p>NEW: Filters to "Read more" link text and URL.</p>
        <p>NEW: Filter to form redirect URL.</p>
        <p>NEW: Filter to skip prerender (for compatibility).</p>
        <p>NEW: Filters for styles, scripts and script vars.</p>
        <p>Minor refactoring for improved performance.</p>
    </div>
</div>
