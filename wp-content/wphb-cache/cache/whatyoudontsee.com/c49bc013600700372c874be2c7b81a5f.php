<?php die(); ?><!-- This page is cached by the Hummingbird Performance plugin v1.7.2 - https://wordpress.org/plugins/hummingbird-performance/. --><!DOCTYPE html>
<html lang="en-US">
<head>
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta charset="UTF-8">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="http://whatyoudontsee.com/xmlrpc.php">
<title>What You Don&#039;t See</title>
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="What You Don&#039;t See &raquo; Feed" href="http://whatyoudontsee.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="What You Don&#039;t See &raquo; Comments Feed" href="http://whatyoudontsee.com/comments/feed/" />
<link rel="alternate" type="text/calendar" title="What You Don&#039;t See &raquo; iCal Feed" href="http://whatyoudontsee.com/upcoming-events/?ical=1" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/whatyoudontsee.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.4"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56692,8205,9792,65039],[55357,56692,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='woocommerce-layout-css'  href='http://whatyoudontsee.com/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=3.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-smallscreen-css'  href='http://whatyoudontsee.com/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=3.3.1' type='text/css' media='only screen and (max-width: 768px)' />
<link rel='stylesheet' id='woocommerce-general-css'  href='http://whatyoudontsee.com/wp-content/plugins/woocommerce/assets/css/woocommerce.css?ver=3.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='fluida-themefonts-css'  href='http://whatyoudontsee.com/wp-content/themes/fluida/resources/fonts/fontfaces.css?ver=1.5.0.2' type='text/css' media='all' />
<link rel='stylesheet' id='fluida-googlefonts-css'  href='//fonts.googleapis.com/css?family=Open+Sans%7COpen+Sans+Condensed300%7COpen+Sans%3A300%7COpen+Sans+Condensed%3A300%2C300%7COpen+Sans%3A700&#038;ver=1.5.0.2' type='text/css' media='all' />
<link rel='stylesheet' id='fluida-main-css'  href='http://whatyoudontsee.com/wp-content/themes/fluida/style.css?ver=1.5.0.2' type='text/css' media='all' />
<style id='fluida-main-inline-css' type='text/css'>
 #site-header-main-inside, #container, #colophon-inside, #footer-inside, #breadcrumbs-container-inside, #wp-custom-header { margin: 0 auto; max-width: 1920px; } #site-header-main { left: 0; right: 0; } #primary { width: 280px; } #secondary { width: 280px; } #container.one-column { } #container.two-columns-right #secondary { float: right; } #container.two-columns-right .main, .two-columns-right #breadcrumbs { width: calc( 98% - 280px ); float: left; } #container.two-columns-left #primary { float: left; } #container.two-columns-left .main, .two-columns-left #breadcrumbs { width: calc( 98% - 280px ); float: right; } #container.three-columns-right #primary, #container.three-columns-left #primary, #container.three-columns-sided #primary { float: left; } #container.three-columns-right #secondary, #container.three-columns-left #secondary, #container.three-columns-sided #secondary { float: left; } #container.three-columns-right #primary, #container.three-columns-left #secondary { margin-left: 2%; margin-right: 2%; } #container.three-columns-right .main, .three-columns-right #breadcrumbs { width: calc( 96% - 560px ); float: left; } #container.three-columns-left .main, .three-columns-left #breadcrumbs { width: calc( 96% - 560px ); float: right; } #container.three-columns-sided #secondary { float: right; } #container.three-columns-sided .main, .three-columns-sided #breadcrumbs { width: calc( 96% - 560px ); float: right; margin: 0 calc( 2% + 280px ) 0 -1920px; } html { font-family: Open Sans; font-size: 16px; font-weight: 300; line-height: 1.8; } #site-title { font-family: Open Sans Condensed; font-size: 150%; font-weight: 300; } #access ul li a { font-family: Open Sans Condensed; font-size: 105%; font-weight: 300; } #access i.search-icon { font-size: 105%; } .widget-title { font-family: Open Sans; font-size: 100%; font-weight: 700; } .widget-container { font-family: Open Sans; font-size: 100%; font-weight: 300; } .entry-title, #reply-title { font-family: Open Sans; font-size: 250%; font-weight: 300; } .content-masonry .entry-title { font-size: 187.5%; } h1 { font-size: 3.029em; } h2 { font-size: 2.678em; } h3 { font-size: 2.327em; } h4 { font-size: 1.976em; } h5 { font-size: 1.625em; } h6 { font-size: 1.274em; } h1, h2, h3, h4, h5, h6 { font-family: Open Sans Condensed; font-weight: 300; } body { color: #ffffff; background-color: #282828; } #site-header-main, #site-header-main-inside, #access ul li a, #access ul ul, .menu-search-animated .searchform input[type="search"], #access::after { background-color: #2d2d2d; } #access .menu-main-search .searchform { border-color: #d1d1d1; } #header a { color: #4682b4; } #access > div > ul > li, #access > div > ul > li > a { color: #d1d1d1; } #access ul.sub-menu li a, #access ul.children li a { color: #555; } #access ul.sub-menu li:hover > a, #access ul.children li:hover > a { background-color: rgba(85,85,85,0.1); } #access > div > ul > li:hover > a { color: #2d2d2d; } #access ul > li.current_page_item > a, #access ul > li.current-menu-item > a, #access ul > li.current_page_ancestor > a, #access ul > li.current-menu-ancestor > a, #access .sub-menu, #access .children { border-top-color: #d1d1d1; } #access ul ul ul { border-left-color: rgba(85,85,85,0.5); } #access > div > ul > li.menu-main-search:hover, #access > div > ul > li:hover > a { background-color: #d1d1d1; } #access ul.children > li.current_page_item > a, #access ul.sub-menu > li.current-menu-item > a, #access ul.children > li.current_page_ancestor > a, #access ul.sub-menu > li.current-menu-ancestor > a { border-color: #555; } .searchform .searchsubmit, .searchform:hover input[type="search"], .searchform input[type="search"]:focus { color: #545454; background-color: transparent; } .searchform::after, .searchform input[type="search"]:focus, .searchform .searchsubmit:hover { background-color: #4682b4; } article.hentry, #primary, .searchform, .main > div:not(#content-masonry), .main > header, .main > nav#nav-below, .pagination span, .pagination a, #nav-old-below .nav-previous, #nav-old-below .nav-next { background-color: #545454; } #breadcrumbs-container { background-color: #343434;} #secondary { background-color: #545454; } #colophon, #footer { background-color: #2d2d2d; color: #d1d1d1; } span.entry-format { color: #4682b4; } .format-aside { border-top-color: #282828; } article.hentry .post-thumbnail-container { background-color: rgba(255,255,255,0.15); } .entry-content blockquote::before, .entry-content blockquote::after { color: rgba(255,255,255,0.1); } .entry-content h1, .entry-content h2, .entry-content h3, .entry-content h4 { color: #ffffff; } a { color: #4682b4; } a:hover, .entry-meta span a:hover, .comments-link a:hover { color: #dc143c; } #footer a, .page-title strong { color: #4682b4; } #footer a:hover, #site-title a:hover span { color: #dc143c; } #access > div > ul > li.menu-search-animated:hover i { color: #2d2d2d; } .continue-reading-link { color: #545454; background-color: #dc143c} .continue-reading-link:before { background-color: #4682b4} .continue-reading-link:hover { color: #545454; } header.pad-container { border-top-color: #4682b4; } article.sticky:after { background-color: rgba(70,130,180,1); } .socials a:before { color: #4682b4; } .socials a:hover:before { color: #dc143c; } .fluida-normalizedtags #content .tagcloud a { color: #545454; background-color: #4682b4; } .fluida-normalizedtags #content .tagcloud a:hover { background-color: #dc143c; } #toTop .icon-back2top:before { color: #4682b4; } #toTop:hover .icon-back2top:before { color: #dc143c; } .entry-meta .icon-metas:before { color: #dc143c; } .page-link a:hover { border-top-color: #dc143c; } #site-title span a span:nth-child(1) { background-color: #dc143c; color: #2d2d2d; width: 1.2em; margin-right: .1em; text-align: center; line-height: 1.2; font-weight: bold; } .fluida-caption-one .main .wp-caption .wp-caption-text { border-bottom-color: #656565; } .fluida-caption-two .main .wp-caption .wp-caption-text { background-color: #5e5e5e; } .fluida-image-one .entry-content img[class*="align"], .fluida-image-one .entry-summary img[class*="align"], .fluida-image-two .entry-content img[class*='align'], .fluida-image-two .entry-summary img[class*='align'] { border-color: #656565; } .fluida-image-five .entry-content img[class*='align'], .fluida-image-five .entry-summary img[class*='align'] { border-color: #4682b4; } /* diffs */ span.edit-link a.post-edit-link, span.edit-link a.post-edit-link:hover, span.edit-link .icon-edit:before { color: #bababa; } .searchform { border-color: #686868; } .entry-meta span, .entry-utility span, .entry-meta time, .comment-meta a, #breadcrumbs-nav .icon-angle-right::before, .footermenu ul li span.sep { color: #bababa; } #footer { border-top-color: #414141; } #colophon .widget-container:after { background-color: #414141; } #commentform { max-width:650px;} code, .reply a:after, #nav-below .nav-previous a:before, #nav-below .nav-next a:before, .reply a:after { background-color: #656565; } pre, .entry-meta .author, nav.sidebarmenu, .page-link > span, article #author-info, .comment-author, .commentlist .comment-body, .commentlist .pingback, nav.sidebarmenu li a { border-color: #656565; } select, input[type], textarea { color: #ffffff; } button, input[type="button"], input[type="submit"], input[type="reset"] { background-color: #4682b4; color: #545454; } button:hover, input[type="button"]:hover, input[type="submit"]:hover, input[type="reset"]:hover { background-color: #dc143c; } select, input[type], textarea { border-color: #6a6a6a; } input[type]:hover, textarea:hover, select:hover, input[type]:focus, textarea:focus, select:focus { border-color: #868686; } hr { background-color: #6a6a6a; } #toTop { background-color: rgba(89,89,89,0.8) } /* woocommerce */ .woocommerce-page #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce-page button.button.alt, .woocommerce input.button.alt, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button { background-color: #4682b4; color: #545454; line-height: 1.8; } .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover { background-color: #68a4d6; color: #545454;} .woocommerce-page #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce-page button.button.alt, .woocommerce input.button.alt { background-color: #dc143c; color: #545454; line-height: 1.8; } .woocommerce-page #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce-page button.button.alt:hover, .woocommerce input.button.alt:hover { background-color: #fe365e; color: #545454;} .woocommerce div.product .woocommerce-tabs ul.tabs li.active { border-bottom-color: #545454; } .woocommerce #respond input#submit.alt.disabled, .woocommerce #respond input#submit.alt.disabled:hover, .woocommerce #respond input#submit.alt:disabled, .woocommerce #respond input#submit.alt:disabled:hover, .woocommerce #respond input#submit.alt[disabled]:disabled, .woocommerce #respond input#submit.alt[disabled]:disabled:hover, .woocommerce a.button.alt.disabled, .woocommerce a.button.alt.disabled:hover, .woocommerce a.button.alt:disabled, .woocommerce a.button.alt:disabled:hover, .woocommerce a.button.alt[disabled]:disabled, .woocommerce a.button.alt[disabled]:disabled:hover, .woocommerce button.button.alt.disabled, .woocommerce button.button.alt.disabled:hover, .woocommerce button.button.alt:disabled, .woocommerce button.button.alt:disabled:hover, .woocommerce button.button.alt[disabled]:disabled, .woocommerce button.button.alt[disabled]:disabled:hover, .woocommerce input.button.alt.disabled, .woocommerce input.button.alt.disabled:hover, .woocommerce input.button.alt:disabled, .woocommerce input.button.alt:disabled:hover, .woocommerce input.button.alt[disabled]:disabled, .woocommerce input.button.alt[disabled]:disabled:hover { background-color: #dc143c; } .woocommerce ul.products li.product .price, .woocommerce div.product p.price, .woocommerce div.product span.price { color: #ffffff } #add_payment_method #payment, .woocommerce-cart #payment, .woocommerce-checkout #payment { background: #5e5e5e; } .woocommerce .main .page-title { font-size: 117em; } /* mobile menu */ nav#mobile-menu { background-color: #2d2d2d; } #mobile-menu .mobile-arrow { color: ; } .main .entry-content, .main .entry-summary { text-align: Default; } .main p, .main ul, .main ol, .main dd, .main pre, .main hr { margin-bottom: 1.0em; } .main p { text-indent: 0.0em;} .main a.post-featured-image { background-position: center top; } #content { margin-top: 20px; } #content { padding-left: 0px; padding-right: 0px; } #header-widget-area { width: 33%; right: 10px; } .fluida-stripped-table .main thead th { border-bottom-color: #6a6a6a; } .fluida-stripped-table .main td, .fluida-stripped-table .main th { border-top-color: #6a6a6a; } .fluida-bordered-table .main th, .fluida-bordered-table .main td { border-color: #6a6a6a; } .fluida-stripped-table .main tr:nth-child(even) td { background-color: #5d5d5d; } .fluida-cropped-featured .main .post-thumbnail-container { height: 200px; } .fluida-responsive-featured .main .post-thumbnail-container { max-height: 200px; height: auto; } article.hentry .article-inner, #breadcrumbs-nav, body.woocommerce.woocommerce-page #breadcrumbs-nav, #content-masonry article.hentry .article-inner, .pad-container { padding-left: 10%; padding-right: 10%; } .fluida-magazine-two.archive #breadcrumbs-nav, .fluida-magazine-two.archive .pad-container, .fluida-magazine-two.search #breadcrumbs-nav, .fluida-magazine-two.search .pad-container, .fluida-magazine-two.page-template-template-page-with-intro #breadcrumbs-nav, .fluida-magazine-two.page-template-template-page-with-intro .pad-container { padding-left: 5%; padding-right: 5%; } .fluida-magazine-three.archive #breadcrumbs-nav, .fluida-magazine-three.archive .pad-container, .fluida-magazine-three.search #breadcrumbs-nav, .fluida-magazine-three.search .pad-container, .fluida-magazine-three.page-template-template-page-with-intro #breadcrumbs-nav, .fluida-magazine-three.page-template-template-page-with-intro .pad-container { padding-left: 3.3333333333333%; padding-right: 3.3333333333333%; } #site-header-main { height:100px; } .menu-search-animated, #sheader, .identity, #nav-toggle { height:100px; line-height:100px; } #access div > ul > li > a { line-height:98px; } #branding { height:100px; } .fluida-responsive-headerimage #masthead #header-image-main-inside { max-height: 2000px; } .fluida-cropped-headerimage #masthead div.header-image { height: 2000px; } #masthead #site-header-main { position: fixed; top: 0; box-shadow: 0 0 3px rgba(0,0,0,0.2); } #header-image-main { margin-top: 100px; } .fluida-landing-page .lp-blocks-inside, .fluida-landing-page .lp-boxes-inside, .fluida-landing-page .lp-text-inside, .fluida-landing-page .lp-posts-inside, .fluida-landing-page .lp-section-header { max-width: 1920px; } .seriousslider-theme .seriousslider-caption-buttons a:nth-child(2n+1), a.staticslider-button:nth-child(2n+1) { color: #ffffff; border-color: #545454; background-color: #545454; } .seriousslider-theme .seriousslider-caption-buttons a:nth-child(2n+1):hover, a.staticslider-button:nth-child(2n+1):hover { color: #545454; } .seriousslider-theme .seriousslider-caption-buttons a:nth-child(2n), a.staticslider-button:nth-child(2n) { border-color: #545454; color: #545454; } .seriousslider-theme .seriousslider-caption-buttons a:nth-child(2n):hover, .staticslider-button:nth-child(2n):hover { color: #ffffff; background-color: #545454; } .lp-blocks { background-color: #545454; } .lp-block > i::before { color: #4682b4; } .lp-block:hover i::before { color: #dc143c; } .lp-block i:after { background-color: #4682b4; } .lp-block:hover i:after { background-color: #dc143c; } .lp-block-text, .lp-boxes-static .lp-box-text, .lp-section-desc { color: #c3c3c3; } .lp-text { background-color: #545454; } .lp-boxes-1 .lp-box .lp-box-image { height: 250px; } .lp-boxes-1.lp-boxes-animated .lp-box:hover .lp-box-text { max-height: 150px; } .lp-boxes-2 .lp-box .lp-box-image { height: 400px; } .lp-boxes-2.lp-boxes-animated .lp-box:hover .lp-box-text { max-height: 300px; } .lp-box-readmore { color: #4682b4; } .lp-boxes .lp-box-overlay { background-color: rgba(70,130,180, 0.9); } .lpbox-rnd1 { background-color: #8b8b8b; } .lpbox-rnd2 { background-color: #909090; } .lpbox-rnd3 { background-color: #959595; } .lpbox-rnd4 { background-color: #9a9a9a; } .lpbox-rnd5 { background-color: #9f9f9f; } .lpbox-rnd6 { background-color: #a4a4a4; } .lpbox-rnd7 { background-color: #a9a9a9; } .lpbox-rnd8 { background-color: #aeaeae; } 
/* Fluida Custom CSS */
</style>
<link rel='stylesheet' id='wpmtst-custom-style-css'  href='http://whatyoudontsee.com/wp-content/plugins/strong-testimonials/public/css/custom.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='um_minified-css'  href='http://whatyoudontsee.com/wp-content/plugins/ultimate-member/assets/css/um.min.css?ver=1.3.88' type='text/css' media='all' />
<link rel='stylesheet' id='wpdevelop-bts-css'  href='http://whatyoudontsee.com/wp-content/plugins/booking/assets/libs/bootstrap/css/bootstrap.css?ver=3.3.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='wpdevelop-bts-theme-css'  href='http://whatyoudontsee.com/wp-content/plugins/booking/assets/libs/bootstrap/css/bootstrap-theme.css?ver=3.3.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='wpbc-client-pages-css'  href='http://whatyoudontsee.com/wp-content/plugins/booking/css/client.css?ver=8.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='wpbc-admin-timeline-css'  href='http://whatyoudontsee.com/wp-content/plugins/booking/css/timeline.css?ver=8.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='wpbc-calendar-css'  href='http://whatyoudontsee.com/wp-content/plugins/booking/css/calendar.css?ver=8.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='wpbc-calendar-skin-css'  href='http://whatyoudontsee.com/wp-content/plugins/booking/css/skins/traditional.css?ver=8.1.1' type='text/css' media='all' />
<script type='text/javascript' src='http://whatyoudontsee.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='http://whatyoudontsee.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='http://whatyoudontsee.com/wp-content/themes/fluida/resources/js/html5shiv.min.js?ver=1.5.0.2'></script>
<![endif]-->
<script type='text/javascript'>
/* <![CDATA[ */
var wpbc_global1 = {"wpbc_ajaxurl":"http:\/\/whatyoudontsee.com\/wp-admin\/admin-ajax.php","wpdev_bk_plugin_url":"http:\/\/whatyoudontsee.com\/wp-content\/plugins\/booking","wpdev_bk_today":"[2018,2,12,18,2]","visible_booking_id_on_page":"[]","booking_max_monthes_in_calendar":"1y","user_unavilable_days":"[999]","wpdev_bk_edit_id_hash":"","wpdev_bk_plugin_filename":"wpdev-booking.php","bk_days_selection_mode":"multiple","wpdev_bk_personal":"0","block_some_dates_from_today":"0","message_verif_requred":"This field is required","message_verif_requred_for_check_box":"This checkbox must be checked","message_verif_requred_for_radio_box":"At least one option must be selected","message_verif_emeil":"Incorrect email field","message_verif_same_emeil":"Your emails do not match","message_verif_selectdts":"Please, select booking date(s) at Calendar.","parent_booking_resources":"[]","new_booking_title":"Thank you for your online booking.  We will send confirmation of your booking as soon as possible.","new_booking_title_time":"","type_of_thank_you_message":"message","thank_you_page_URL":"http:\/\/whatyoudontsee.com\/thank-you","is_am_pm_inside_time":"false","is_booking_used_check_in_out_time":"false","wpbc_active_locale":"en_US","wpbc_message_processing":"Processing","wpbc_message_deleting":"Deleting","wpbc_message_updating":"Updating","wpbc_message_saving":"Saving"};
/* ]]> */
</script>
<script type='text/javascript' src='http://whatyoudontsee.com/wp-content/plugins/booking/js/wpbc_vars.js?ver=8.1.1'></script>
<script type='text/javascript' src='http://whatyoudontsee.com/wp-content/plugins/booking/assets/libs/bootstrap/js/bootstrap.js?ver=3.3.5.1'></script>
<script type='text/javascript' src='http://whatyoudontsee.com/wp-content/plugins/booking/js/datepick/jquery.datepick.js?ver=1.1'></script>
<script type='text/javascript' src='http://whatyoudontsee.com/wp-content/plugins/booking/js/client.js?ver=8.1.1'></script>
<link rel='https://api.w.org/' href='http://whatyoudontsee.com/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://whatyoudontsee.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://whatyoudontsee.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.4" />
<meta name="generator" content="WooCommerce 3.3.1" />
<link rel='shortlink' href='http://whatyoudontsee.com/' />
<link rel="alternate" type="application/json+oembed" href="http://whatyoudontsee.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwhatyoudontsee.com%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://whatyoudontsee.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwhatyoudontsee.com%2F&#038;format=xml" />
<meta name="tec-api-version" content="v1"><meta name="tec-api-origin" content="http://whatyoudontsee.com"><link rel="https://theeventscalendar.com/" href="http://whatyoudontsee.com/wp-json/tribe/events/v1/" />	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
	 
		<script type="text/javascript">

		var ultimatemember_image_upload_url = 'http://whatyoudontsee.com/wp-content/plugins/ultimate-member/core/lib/upload/um-image-upload.php';
		var ultimatemember_file_upload_url = 'http://whatyoudontsee.com/wp-content/plugins/ultimate-member/core/lib/upload/um-file-upload.php';
		var ultimatemember_ajax_url = 'http://whatyoudontsee.com/wp-admin/admin-ajax.php';

		</script>

	
		<style type="text/css">.request_name { display: none !important; }</style>

			<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<style type="text/css" id="custom-background-css">
body.custom-background { background-image: url("http://whatyoudontsee.com/wp-content/uploads/2018/02/michal-grosicki-235026.jpg"); background-position: center center; background-size: cover; background-repeat: no-repeat; background-attachment: fixed; }
</style>
		<style type="text/css" id="wp-custom-css">
			/* Remove bottom border on images */
.fluida-image-one .entry-content img[class*='align'], .fluida-image-one .entry-summary img[class*='align'] {
	border: 0;
}


/* Change color of main menu dropdowns */
#access ul.sub-menu li a, #access ul.children li a{
	color: #d1d1d1;
}
/* 
Change header color on calendar */
#tribe-geo-results h2.tribe-events-page-title, h2.tribe-events-page-title{
	color: white;
}

/* Change button color on calendar */
.tribe-events-calendar td.tribe-events-past div[id*="tribe-events-daynum-"], .tribe-events-calendar td.tribe-events-past div[id*="tribe-events-daynum-"] > a{
	background-color: #A9A9A9;
}

/* Change link colors */
a{
	font-weight: bold;
	color: #4682B4;
}

/* Calendar Events */
.single-tribe_events .tribe-events-event-meta{
	color: black;
}


/* Change home page button colors */
.seriousslider-theme .seriousslider-caption-buttons a:nth-child(2n+1), a.staticslider-button:nth-child(2n+1){
	background-color: #2d2d2d;;
	border-color: #2d2d2d;;
	color: white;
}

.seriousslider-theme .seriousslider-caption-buttons a:nth-child(2n+1), a.staticslider-button:nth-child(2n+1):hover{
	color: #2d2d2d;
}

.seriousslider-theme .seriousslider-caption-buttons a:nth-child(2n), a.staticslider-button:nth-child(2n){
	border-color: #2d2d2d;
	color: #2d2d2d;
}

.seriousslider-theme .seriousslider-caption-buttons a:nth-child(2n), a.staticslider-button:nth-child(2n):hover{
	background-color: #2d2d2d;
	color: white;
}

/* Change calendar search bar label colors */
#tribe-bar-form .tribe-bar-views-inner label{
	color: black;
}

#tribe-bar-form label{
	color: black;
}

/* Change header color for individual events */
.tribe-events-list-separator-month span{
	background-color: white;
	color: #2d2d2d;
}

/* Change the hover colors for the "view as" links on the calendar search bar */
#tribe-bar-views .tribe-bar-views-list .tribe-bar-views-option.tribe-bar-active a:hover{
	color: white;
}

#tribe-bar-views .tribe-bar-views-list .tribe-bar-views-option a:hover{
	color: white;
}
/* Change Events Styles */
#tribe-bar-form {
	background:rgba(245,245,245,.07);
}
#tribe-bar-collapse-toggle{
	background:rgba(245,245,245,.07);
}
.tribe-events-loop .tribe-events-event-meta {
	background:rgba(245,245,245,.07);
	border:none;
	padding:5px;
}

/* Hides the header image on event detail page */
#masthead .header-image{
	display: none;
}
/* 
Account page labels */
.um-account-main p, .um-account-main div.um-account-heading, .um-field-label label, .um-field-block{
	color: white;
}

/* Add padding to content pages */
#content{
	padding: 0 100px 0 100px
}

/* Remove padding from Home page */
.fluida-landing-page #content{
	padding: 0;
}

/* 
Hide the register button on login page */
.um-right{
	display: none;
}

.caldera-grid .has-error .checkbox, .caldera-grid .has-error .checkbox-inline, .caldera-grid .has-error .control-label, .caldera-grid .has-error .form-control-feedback, .caldera-grid .has-error .help-block, .caldera-grid .has-error .radio, .caldera-grid .has-error .radio-inline, .caldera-grid .has-error.checkbox label, .caldera-grid .has-error.checkbox-inline label, .caldera-grid .has-error.radio label, .caldera-grid .has-error.radio-inline label{
	color : red;
	font-weight: bold;
}

/* Change submit testimonial button text color */
button, input[type="button"], input[type="submit"], input[type="reset"]{
	color: white;
}

/* Change the hover color for the submenu in the navbar */
#access ul.sub-menu li a:hover, #access ul.children li a:hover{
	background-color: #d1d1d1;
	color: #555;
}

#access .menu-main-search i.search-icon:hover{
	color: white;
}

/* Modify the about section on homepage */
.main .lp-text-title{
	display: none;
}

.lp-text{
	padding: 30px 200px 30px 200px;
}

.lp-text-overlay + .lp-text-inside{
	background-color: rgba(0, 0, 0, .4);
	padding-bottom: 40px;
	border-radius: 3px;
}


/* Modify the testimonial viewer on homepage */
.lp-text{
	background-color: #1a1a1a;
	padding-bottom: 70px;
}

.strong-view.modern *{
	color: #d9d9d9;
}

.wpbc_booking_form_structure.wpbc_form_dark .wpbc_structure_form input[type="text"], .wpbc_booking_form_structure.wpbc_form_dark .wpbc_structure_form textarea, .wpbc_booking_form_structure.wpbc_form_dark .wpbc_structure_form select{
	color: #d1d1d1;
}

.lp-boxes-animated .lp-box-title{
	background-color: rgba(0,0,0,0.6);
	padding: 7px;
	border-radius: 8px;
}

/* Modify booking calendar colors */
.fluida-stripped-table .main tr:nth-child(even) td.datepick-unselectable, .datepick-days-cell.datepick-unselectable{
	background-color: #4d4d4d;
		color: white;
}

.datepick-unselectable.date_approved{
	background-color: #C60B0B;
	color: #5F0000;
	text-shadow: none;
}

.block_hints .date_approved.block_check_in_out, .block_hints .block_booked, .datepick-inline .date_approved, .block_hints .block_booked a, .datepick-inline .date_approved a{
	text-shadow: none;
}

.fluida-stripped-table .main tr:nth-child(even) td{
	background-color: white;
}

.datepick-unselectable.date2approve{
	background-color: #DF9A00;
	color: #885500;
}

/* Shop - Woocommerce Styles */
.woocommerce .main .page-title {
	font-size:24pt!important;
	font-weight:bold;
	font-family:'Georgia';
}

/* Caldera - Modify Form Styles */
.caldera-grid .btn {
    display: block !important;
    margin-left: auto;
    margin-right: auto;
}
.caldera-grid .btn {
    width:100%
}
.caldera-grid .btn {
    background-color:rgb(70, 130, 180);
    color:white;
}
.caldera-grid .btn:hover {
    background-color:#dc143c;
		color:white;
}


/* Screens less than 768px removes padding around main content box - mobile friendly */
@media only screen and (max-width: 768px){
	.lp-text{
		padding: 30px 0px;
	}
	#content{
		padding: 30px 0px;
	}
	.tribe-events-loop .tribe-events-event-meta{
background:rgba(245,245,245,.07);
	border:none;
	padding:5px;
	}
}	

/* Screens less than 480px removes excess padding around testimonials - mobile friendly */
@media only screen and (max-width: 480px){
	.strong-view.modern.controls-type-sides .testimonial-inner {
    padding-left: 0px;
    padding-right: 0px;
	}
	.lp-text{
		padding: 0px;
	}
	.lp-text-content p{
		padding-left:10px!important;
	}
	.entry-content p{
		padding-left:0px!important;
	}
}		</style>
	</head>

<body class="home page-template-default page page-id-533 custom-background tribe-no-js fluida-landing-page fluida-image-one fluida-caption-two fluida-totop-normal fluida-stripped-table fluida-fixed-menu fluida-responsive-headerimage fluida-responsive-featured fluida-magazine-two fluida-magazine-layout fluida-comment-placeholder fluida-hide-page-title fluida-elementshadow fluida-normalizedtags fluida-article-animation-1 fluida-menu-animation" itemscope itemtype="http://schema.org/WebPage">
	
	<header id="masthead" class="cryout"  itemscope itemtype="http://schema.org/WPHeader" role="banner">

		<div id="site-header-main">
			<div id="site-header-main-inside">

				<nav id="mobile-menu">
					<span id="nav-cancel"><i class="blicon-cross3"></i></span>
					<div><ul id="mobile-nav" class=""><li id="menu-item-138" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-138"><a href="http://whatyoudontsee.com"><span>Home</span></a></li>
<li id="menu-item-531" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-531"><a><span>About</span></a>
<ul class="sub-menu">
	<li id="menu-item-135" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-135"><a href="http://whatyoudontsee.com/about/"><span>Our Mission</span></a></li>
	<li id="menu-item-136" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-136"><a href="http://whatyoudontsee.com/our-sponsors/"><span>Our Sponsors</span></a>
	<ul class="sub-menu">
		<li id="menu-item-732" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-732"><a href="http://iowanarcs.com/"><span>Iowa Narcotics Officers Association</span></a></li>
		<li id="menu-item-733" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-733"><a href="https://www.sammonsfinancialgroup.com/"><span>Sammons Financial Group</span></a></li>
	</ul>
</li>
</ul>
</li>
<li id="menu-item-493" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-493"><a><span>Where to Find Us</span></a>
<ul class="sub-menu">
	<li id="menu-item-734" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-734"><a href="http://whatyoudontsee.com/upcoming-events/"><span>Upcoming Events</span></a></li>
	<li id="menu-item-161" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-161"><a href="http://whatyoudontsee.com/previous-events/"><span>Previous Events</span></a></li>
</ul>
</li>
<li id="menu-item-496" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-496"><a><span>Resources</span></a>
<ul class="sub-menu">
	<li id="menu-item-164" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-164"><a href="http://whatyoudontsee.com/warning-signs/"><span>Warning Signs</span></a></li>
	<li id="menu-item-166" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-166"><a href="http://whatyoudontsee.com/leo-contact-tip-lines/"><span>LE Tip Lines</span></a></li>
	<li id="menu-item-678" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-678"><a href="http://whatyoudontsee.com/prevention-community-coalition-links/"><span>Community Coalition Links</span></a></li>
	<li id="menu-item-718" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-718"><a href="http://whatyoudontsee.com/printable-resources/"><span>Printable Resources</span></a></li>
</ul>
</li>
<li id="menu-item-497" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-497"><a><span>Contact Us</span></a>
<ul class="sub-menu">
	<li id="menu-item-367" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-367"><a href="http://whatyoudontsee.com/contact-to-schedule/"><span>Schedule an Event</span></a></li>
	<li id="menu-item-370" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-370"><a href="http://whatyoudontsee.com/newsletter-mailing-list/"><span>Join the Newsletter</span></a></li>
	<li id="menu-item-446" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-446"><a href="http://whatyoudontsee.com/submit-testimonial/"><span>Submit a Testimonial</span></a></li>
	<li id="menu-item-369" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-369"><a href="http://whatyoudontsee.com/info-request-question-form/"><span>Request Information</span></a></li>
	<li id="menu-item-371" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-371"><a href="http://whatyoudontsee.com/media-inquiries/"><span>Media Inquiries</span></a></li>
</ul>
</li>
<li id="menu-item-564" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-564"><a href="http://whatyoudontsee.com/shop/"><span>Shop</span></a>
<ul class="sub-menu">
	<li id="menu-item-661" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-661"><a href="http://whatyoudontsee.com/?post_type=product"><span>Store</span></a></li>
	<li id="menu-item-605" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-605"><a href="http://whatyoudontsee.com/cart-page/"><span>Cart</span></a></li>
	<li id="menu-item-660" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-660"><a href="http://whatyoudontsee.com/checkout-page/"><span>Checkout</span></a></li>
</ul>
</li>
<li id="menu-item-401" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-401"><a href="http://whatyoudontsee.com/login/"><span>Login</span></a>
<ul class="sub-menu">
	<li id="menu-item-454" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-454"><a href="http://whatyoudontsee.com/register/"><span>Register</span></a></li>
</ul>
</li>
<li class='menu-main-search menu-search-animated'><i class='search-icon'></i>
<form role="search" method="get" class="searchform" action="http://whatyoudontsee.com/">
	<label>
		<span class="screen-reader-text">Search for:</span>
		<input type="search" class="s" placeholder="Search" value="" name="s" />
	</label>
	<button type="submit" class="searchsubmit"><span class="screen-reader-text">Search</span><i class="blicon-magnifier"></i></button>
</form>
 </li></ul></div>				</nav> <!-- #mobile-menu -->

				<div id="branding">
					<div id="site-text"><h1 itemprop="headline" id="site-title"><span> <a href="http://whatyoudontsee.com/" title="" rel="home">What You Don&#039;t See</a> </span></h1><span id="site-description"  itemprop="description" ></span></div>				</div><!-- #branding -->

				<nav id="sheader" class="socials"><a target="_blank" href="http://www.facebook.com/profile"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-108"><span>Facebook</span></a><a target="_blank" href="http://www.twitter.com/profile"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-109"><span>Twitter</span></a><a target="_blank" href="http://plus.google.com/profile"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-110"><span>Google Plus</span></a><a href="https://www.instagram.com/?hl=en"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-466"><span>Instagram</span></a></nav>
				<a id="nav-toggle"><span>&nbsp;</span></a>
				<nav id="access" role="navigation"  aria-label="Primary Menu"  itemscope itemtype="http://schema.org/SiteNavigationElement">
						<div class="skip-link screen-reader-text">
		<a href="#main" title="Skip to content"> Skip to content </a>
	</div>
	<div><ul id="prime_nav" class=""><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-138"><a href="http://whatyoudontsee.com"><span>Home</span></a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-531"><a><span>About</span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-135"><a href="http://whatyoudontsee.com/about/"><span>Our Mission</span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-136"><a href="http://whatyoudontsee.com/our-sponsors/"><span>Our Sponsors</span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-732"><a href="http://iowanarcs.com/"><span>Iowa Narcotics Officers Association</span></a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-733"><a href="https://www.sammonsfinancialgroup.com/"><span>Sammons Financial Group</span></a></li>
	</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-493"><a><span>Where to Find Us</span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-734"><a href="http://whatyoudontsee.com/upcoming-events/"><span>Upcoming Events</span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-161"><a href="http://whatyoudontsee.com/previous-events/"><span>Previous Events</span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-496"><a><span>Resources</span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-164"><a href="http://whatyoudontsee.com/warning-signs/"><span>Warning Signs</span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-166"><a href="http://whatyoudontsee.com/leo-contact-tip-lines/"><span>LE Tip Lines</span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-678"><a href="http://whatyoudontsee.com/prevention-community-coalition-links/"><span>Community Coalition Links</span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-718"><a href="http://whatyoudontsee.com/printable-resources/"><span>Printable Resources</span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-497"><a><span>Contact Us</span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-367"><a href="http://whatyoudontsee.com/contact-to-schedule/"><span>Schedule an Event</span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-370"><a href="http://whatyoudontsee.com/newsletter-mailing-list/"><span>Join the Newsletter</span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-446"><a href="http://whatyoudontsee.com/submit-testimonial/"><span>Submit a Testimonial</span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-369"><a href="http://whatyoudontsee.com/info-request-question-form/"><span>Request Information</span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-371"><a href="http://whatyoudontsee.com/media-inquiries/"><span>Media Inquiries</span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-564"><a href="http://whatyoudontsee.com/shop/"><span>Shop</span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-661"><a href="http://whatyoudontsee.com/?post_type=product"><span>Store</span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-605"><a href="http://whatyoudontsee.com/cart-page/"><span>Cart</span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-660"><a href="http://whatyoudontsee.com/checkout-page/"><span>Checkout</span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-401"><a href="http://whatyoudontsee.com/login/"><span>Login</span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-454"><a href="http://whatyoudontsee.com/register/"><span>Register</span></a></li>
</ul>
</li>
<li class='menu-main-search menu-search-animated'><i class='search-icon'></i>
<form role="search" method="get" class="searchform" action="http://whatyoudontsee.com/">
	<label>
		<span class="screen-reader-text">Search for:</span>
		<input type="search" class="s" placeholder="Search" value="" name="s" />
	</label>
	<button type="submit" class="searchsubmit"><span class="screen-reader-text">Search</span><i class="blicon-magnifier"></i></button>
</form>
 </li></ul></div>				</nav><!-- #access -->

			</div><!-- #site-header-main-inside -->
		</div><!-- #site-header-main -->

		<div id="header-image-main">
			<div id="header-image-main-inside">
							<div class="header-image"  style="background-image: url(http://whatyoudontsee.com/wp-content/uploads/2018/02/michal-grosicki-235026-1920x1502.jpg)" ></div>
			<img class="header-image" alt="What You Don&#039;t See" src="http://whatyoudontsee.com/wp-content/uploads/2018/02/michal-grosicki-235026-1920x1502.jpg" />
							</div><!-- #header-image-main-inside -->
		</div><!-- #header-image-main -->

	</header><!-- #masthead -->

		
	
	<div id="content" class="cryout">
		
	<div id="container" class="fluida-landing-page one-column">
		<main id="main" role="main" class="main">
		
	<section class="lp-staticslider">
					<img class="lp-staticslider-image" alt="" src="http://whatyoudontsee.com/wp-content/uploads/2017/11/mainBanner.png">
				<div class="staticslider-caption">
												<div class="staticslider-caption-buttons">
											</div>
		</div>
	</section><!-- .lp-staticslider -->

	<section class="lp-text " id="lp-text-one" style="background-image: url( http://whatyoudontsee.com/wp-content/uploads/2018/02/michal-grosicki-235026-e1518374381271.jpg);"  >
		<div class="lp-text-overlay"></div>			<div class="lp-text-inside">
				<h2 class="lp-text-title">Homepage About</h2>				<div class="lp-text-content"><p style="padding-left: 30px;"><span style="color: #ae0001; font-family: 'arial black', sans-serif;"><span style="font-size: 14pt;"><span style="font-size: 18pt;"><strong>What You Don’t See</strong></span></span>  </span><span style="font-size: 14pt; color: #ffffff; font-family: georgia, palatino, serif;">is a mobile training trailer designed to simulate a “typical” teenager’s bedroom, complete with all the paraphernalia and imagery one would expect to see but not to necessarily understand from today’s younger culture. In the room, attendees will learn the meanings of signs, symbols &amp; clothing, common hiding spots and other precursors that can be a tip to otherwise unsuspecting parents.</span></p>
<p>&nbsp;</p>
<p style="padding-left: 30px;"><span style="font-size: 14pt; color: #ffffff; font-family: georgia, palatino, serif;">At the conclusion of the experience, attendees will be provided with valuable resources such as pamphlets, contact information for local law enforcement agencies, substance abuse counselors, specific informational brochures, and much more. The INOA believes this will allow parents and teachers to continue fighting on behalf of their youth with the information gained from the experience in addition to assistance from the resources being pledged towards the project from organizations such as the Office of Drug Control Policy, Midwest HIDTA and more.</span></p>
<p>&nbsp;</p>
<p style="padding-left: 30px;"><span style="font-size: 14pt; color: #ffffff; font-family: georgia, palatino, serif;">This program will educate parents and teachers of youth, primarily pre-teen and teenage at-risk youth, on the warning signs to identify potentially dangerous behaviors and lifestyle choices as well as how to intervene for a more positive outcome on behalf of those young targets before their life choices become terminal decisions. Because drugs, violence and crime don’t discriminate by gender, skin color, religion or affiliation, this public awareness campaign will literally touch Iowans from all walks of life and will be among the largest joint-efforts between city, state and federal agencies Iowa has ever seen.</span></p>
</div>			</div>

	</section><!-- .lp-text-one -->
	<section class="lp-text " id="lp-text-two" >
					<div class="lp-text-inside">
				<h2 class="lp-text-title">Testimonials</h2>				<div class="lp-text-content">
<div class="strong-view strong-view-id-2 modern slider-container slider-mode-fade slider-stretch controls-type-sides controls-style-buttons" data-slider-var=strong_slider_id_2 data-state=idle><div class="strong-content wpmslider-content"><div class="testimonial t-slide post-568"><div class="testimonial-inner"><div class="testimonial-content"><h3 class="testimonial-heading">Please Come Again</h3><p>I can&#8217;t believe how wonderful of an experience this was! Please come again, we will be waiting!</p></div><div class="testimonial-client"><div class="testimonial-name">Stacey Vicks</div></div><div class="clear"></div></div></div><div class="testimonial t-slide post-574"><div class="testimonial-inner"><div class="testimonial-content"><h3 class="testimonial-heading">Good Experience for the Family!</h3><p>We made our way through the trailer while it was stationed at the park in Waterloo. The whole family had a good time, and came away with a deeper understanding.</p></div><div class="testimonial-client"><div class="testimonial-name">Vivian Jackson</div></div><div class="clear"></div></div></div><div class="testimonial t-slide post-572"><div class="testimonial-inner"><div class="testimonial-content"><h3 class="testimonial-heading">Incredibly Informative!</h3><p>I learned so much by participating in this program. Totally worth it!</p></div><div class="testimonial-client"><div class="testimonial-name">Maxwell Clark</div></div><div class="clear"></div></div></div><div class="testimonial t-slide post-567"><div class="testimonial-inner"><div class="testimonial-content"><h3 class="testimonial-heading">Can&#8217;t Wait</h3><p>They did such a fantastic job, so informative! Can&#8217;t wait for them to come again!</p></div><div class="testimonial-client"><div class="testimonial-name">Gary Lee</div></div><div class="clear"></div></div></div><div class="testimonial t-slide post-581"><div class="testimonial-inner"><div class="testimonial-content"><h3 class="testimonial-heading">Fantastic Specialists</h3><p>The specialists we spoke to after we went through the trailer were superb! So informative and enlightening. Don&#8217;t miss this chance!</p></div><div class="testimonial-client"><div class="testimonial-name">Lacey Hendricks</div></div><div class="clear"></div></div></div><div class="testimonial t-slide post-573"><div class="testimonial-inner"><div class="testimonial-content"><h3 class="testimonial-heading">I Learned A Lot!</h3><p>The trailer made an appearance at our school last week, so our class made a pass through. I walked away with a bundle of invaluable information!</p></div><div class="testimonial-client"><div class="testimonial-name">Garrett Betts</div></div><div class="clear"></div></div></div></div></div>


</div>			</div>

	</section><!-- .lp-text-two -->
		<section id="lp-boxes-2" class="lp-boxes lp-boxes-2 lp-boxes-animated lp-boxes-rows-4">
						<div class="												lp-boxes-padding">
    					<div class="lp-box box1 ">
					<div class="lp-box-image lpbox-rnd8">
						<img alt="Happy Holidays!" src="http://whatyoudontsee.com/wp-content/uploads/2017/12/annie-spratt-165447-480x400.jpg" /> 						<a class="lp-box-link" href="http://whatyoudontsee.com/happy-holidays/"><i class="blicon-plus2"></i></a>
						<div class="lp-box-overlay"></div>
					</div>
					<div class="lp-box-content">
						<h5 class="lp-box-title">Happy Holidays!</h5>						<div class="lp-box-text">
															<div class="lp-box-text-inside"> Happy holidays from all of us at What You Don't See! Have a safe and wonderful time with your families! </div>
																						<a class="lp-box-readmore" href="http://whatyoudontsee.com/happy-holidays/" > Read More <i class="icon-angle-right"></i></a>
													</div>
					</div>
			</div><!-- lp-box -->
				<div class="lp-box box2 ">
					<div class="lp-box-image lpbox-rnd1">
						<img alt="Shop Up and Running!" src="http://whatyoudontsee.com/wp-content/uploads/2017/12/coffee_mug_final-480x400.jpg" /> 						<a class="lp-box-link" href="http://whatyoudontsee.com/shop-up-and-running/"><i class="blicon-plus2"></i></a>
						<div class="lp-box-overlay"></div>
					</div>
					<div class="lp-box-content">
						<h5 class="lp-box-title">Shop Up and Running!</h5>						<div class="lp-box-text">
															<div class="lp-box-text-inside"> The What You Don't See shop is finally up and running. You can check it out by following the link in the navigation or by... </div>
																						<a class="lp-box-readmore" href="http://whatyoudontsee.com/shop-up-and-running/" > Read More <i class="icon-angle-right"></i></a>
													</div>
					</div>
			</div><!-- lp-box -->
				<div class="lp-box box3 ">
					<div class="lp-box-image lpbox-rnd5">
						<img alt="Information for the Youth" src="http://whatyoudontsee.com/wp-content/uploads/2017/11/nida_icon.jpg" /> 						<a class="lp-box-link" href="http://whatyoudontsee.com/information-for-the-youth/"><i class="blicon-plus2"></i></a>
						<div class="lp-box-overlay"></div>
					</div>
					<div class="lp-box-content">
						<h5 class="lp-box-title">Information for the Youth</h5>						<div class="lp-box-text">
															<div class="lp-box-text-inside"> National Institute on Drug Abuse for Teens has some of the best up to date information on the latest drugs, substances, and paraphernalia teenagers end... </div>
																						<a class="lp-box-readmore" href="http://whatyoudontsee.com/information-for-the-youth/" > Read More <i class="icon-angle-right"></i></a>
													</div>
					</div>
			</div><!-- lp-box -->
				<div class="lp-box box4 ">
					<div class="lp-box-image lpbox-rnd2">
						<img alt="We Are Under Way!" src="http://whatyoudontsee.com/wp-content/uploads/2017/11/mainBanner.png" /> 						<a class="lp-box-link" href="http://whatyoudontsee.com/hello-world/"><i class="blicon-plus2"></i></a>
						<div class="lp-box-overlay"></div>
					</div>
					<div class="lp-box-content">
						<h5 class="lp-box-title">We Are Under Way!</h5>						<div class="lp-box-text">
															<div class="lp-box-text-inside"> The What You Don't See program is officially under way! The kick-off event was a great success! We couldn't be more excited to bring this... </div>
																						<a class="lp-box-readmore" href="http://whatyoudontsee.com/hello-world/" > Read More <i class="icon-angle-right"></i></a>
													</div>
					</div>
			</div><!-- lp-box -->
				</div>
		</section><!-- .lp-boxes -->
		</main><!-- #main -->
			</div><!-- #container -->

			
		<aside id="colophon" role="complementary"  itemscope itemtype="http://schema.org/WPSideBar">
			<div id="colophon-inside" class="footer-three ">
				
			</div>
		</aside><!-- #colophon -->

	</div><!-- #main -->

	<footer id="footer" class="cryout" role="contentinfo"  itemscope itemtype="http://schema.org/WPFooter">
				<div id="footer-inside">
			<div id="toTop"><i class="icon-back2top"></i> </div><nav id="sfooter" class="socials"><a target="_blank" href="http://www.facebook.com/profile"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-108"><span>Facebook</span></a><a target="_blank" href="http://www.twitter.com/profile"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-109"><span>Twitter</span></a><a target="_blank" href="http://plus.google.com/profile"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-110"><span>Google Plus</span></a><a href="https://www.instagram.com/?hl=en"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-466"><span>Instagram</span></a></nav><div id="site-copyright">&copy; WhatYouDon'tSee</div><div style="display:block;float:right;clear: right;font-size: .9em;">Powered by<a target="_blank" href="http://www.cryoutcreations.eu/wordpress-themes/fluida" title="Fluida WordPress Theme by Cryout Creations"> Fluida</a> &amp; <a target="_blank" href="http://wordpress.org/" title="Semantic Personal Publishing Platform">  WordPress.</a></div>		</div> <!-- #footer-inside -->
	</footer>

	<div id="um_upload_single" style="display:none">
	
</div><div id="um_view_photo" style="display:none">

	<a href="#" data-action="um_remove_modal" class="um-modal-close"><i class="um-faicon-times"></i></a>
	
	<div class="um-modal-body photo">
	
		<div class="um-modal-photo">

		</div>

	</div>
	
</div>		<script>
		( function ( body ) {
			'use strict';
			body.className = body.className.replace( /\btribe-no-js\b/, 'tribe-js' );
		} )( document.body );
		</script>
		<script type='text/javascript'> /* <![CDATA[ */var tribe_l10n_datatables = {"aria":{"sort_ascending":": activate to sort column ascending","sort_descending":": activate to sort column descending"},"length_menu":"Show _MENU_ entries","empty_table":"No data available in table","info":"Showing _START_ to _END_ of _TOTAL_ entries","info_empty":"Showing 0 to 0 of 0 entries","info_filtered":"(filtered from _MAX_ total entries)","zero_records":"No matching records found","search":"Search:","all_selected_text":"All items on this page were selected. ","select_all_link":"Select all pages","clear_selection":"Clear Selection.","pagination":{"all":"All","next":"Next","previous":"Previous"},"select":{"rows":{"0":"","_":": Selected %d rows","1":": Selected 1 row"}},"datepicker":{"dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["January","February","March","April","May","June","July","August","September","October","November","December"],"nextText":"Next","prevText":"Prev","currentText":"Today","closeText":"Done"}};/* ]]> */ </script><link rel='stylesheet' id='testimonials-modern-css'  href='http://whatyoudontsee.com/wp-content/plugins/strong-testimonials/templates/modern/content.css?ver=2.30' type='text/css' media='all' />
<link rel='stylesheet' id='wpmtst-font-awesome-css'  href='http://whatyoudontsee.com/wp-content/plugins/strong-testimonials/public/fonts/font-awesome-4.6.3/css/font-awesome.min.css?ver=4.6.3' type='text/css' media='all' />
<link rel='stylesheet' id='wpmtst-slider-controls-sides-buttons-pager-buttons-css'  href='http://whatyoudontsee.com/wp-content/plugins/strong-testimonials/public/css/slider-controls-sides-buttons-pager-buttons.css?ver=2.30' type='text/css' media='all' />
<script type='text/javascript'>
/* <![CDATA[ */
var cryout_ajax_more = {"page_number_next":"2","page_number_max":"1","page_link_model":"http:\/\/whatyoudontsee.com\/page\/9999999\/","load_more_str":"More Posts","content_css_selector":"#lp-posts .lp-posts-inside","pagination_css_selector":"#lp-posts nav.navigation"};
/* ]]> */
</script>
<script type='text/javascript' src='http://whatyoudontsee.com/wp-content/themes/fluida/resources/js/ajax.js?ver=1.5.0.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"http:\/\/whatyoudontsee.com\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/whatyoudontsee.com\/cart-page\/","is_cart":"","cart_redirect_after_add":"yes"};
/* ]]> */
</script>
<script type='text/javascript' src='http://whatyoudontsee.com/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=3.3.1'></script>
<script type='text/javascript' src='http://whatyoudontsee.com/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
<script type='text/javascript' src='http://whatyoudontsee.com/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"http:\/\/whatyoudontsee.com\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type='text/javascript' src='http://whatyoudontsee.com/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=3.3.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"http:\/\/whatyoudontsee.com\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_08d92200cda56ccc179fabef6229e0e3","fragment_name":"wc_fragments_08d92200cda56ccc179fabef6229e0e3"};
/* ]]> */
</script>
<script type='text/javascript' src='http://whatyoudontsee.com/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=3.3.1'></script>
<script type='text/javascript' src='http://whatyoudontsee.com/wp-includes/js/imagesloaded.min.js?ver=3.2.0'></script>
<script type='text/javascript' defer src='http://whatyoudontsee.com/wp-includes/js/masonry.min.js?ver=3.3.2'></script>
<script type='text/javascript' defer src='http://whatyoudontsee.com/wp-includes/js/jquery/jquery.masonry.min.js?ver=3.1.2b'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var um_scripts = {"ajaxurl":"http:\/\/whatyoudontsee.com\/wp-admin\/admin-ajax.php","fileupload":"http:\/\/whatyoudontsee.com\/wp-content\/plugins\/ultimate-member\/core\/lib\/upload\/um-file-upload.php","imageupload":"http:\/\/whatyoudontsee.com\/wp-content\/plugins\/ultimate-member\/core\/lib\/upload\/um-image-upload.php"};
/* ]]> */
</script>
<script type='text/javascript' src='http://whatyoudontsee.com/wp-content/plugins/ultimate-member/assets/js/um.min.js?ver=1.3.88'></script>
<script type='text/javascript' src='http://whatyoudontsee.com/wp-includes/js/wp-embed.min.js?ver=4.9.4'></script>
<script type='text/javascript' defer src='http://whatyoudontsee.com/wp-content/plugins/strong-testimonials/public/js/lib/actual/jquery.actual.min.js?ver=1.0.16'></script>
<script type='text/javascript' src='http://whatyoudontsee.com/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
<script type='text/javascript' defer src='http://whatyoudontsee.com/wp-content/plugins/strong-testimonials/public/js/lib/verge/verge.min.js?ver=1.10.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var strong_slider_id_2 = {"config":{"mode":"fade","speed":1000,"pause":8000,"autoHover":1,"autoStart":1,"stopAutoOnClick":1,"adaptiveHeight":0,"adaptiveHeightSpeed":500,"controls":1,"autoControls":0,"pager":0,"slideCount":6,"debug":false,"compat":{"flatsome":false},"touchEnabled":true,"stretch":1,"prevText":"","nextText":"","startText":"","stopText":""}};
/* ]]> */
</script>
<script type='text/javascript' defer src='http://whatyoudontsee.com/wp-content/plugins/strong-testimonials/public/js/lib/strongslider/jquery.strongslider.min.js?ver=2.30'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var strongControllerParms = {"method":"","universalTimer":"500","observerTimer":"500","event":"","script":"","containerId":"page","addedNodeId":"content","debug":""};
/* ]]> */
</script>
<script type='text/javascript' src='http://whatyoudontsee.com/wp-content/plugins/strong-testimonials/public/js/controller.min.js?ver=2.30'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fluida_settings = {"masonry":"1","rtl":"","magazine":"2","fitvids":"1","autoscroll":"1","articleanimation":"1","lpboxratios":[1.812,1.2],"is_mobile":""};
/* ]]> */
</script>
<script type='text/javascript' defer src='http://whatyoudontsee.com/wp-content/themes/fluida/resources/js/frontend.js?ver=1.5.0.2'></script>
<script type='text/javascript' defer src='http://whatyoudontsee.com/wp-includes/js/comment-reply.min.js?ver=4.9.4'></script>

		<script type="text/javascript">jQuery( '#request' ).val( '' );</script>

	</body>
</html>
<!-- Hummingbird cache file was created in 0.32373595237732 seconds, on 12-02-18 18:02:19 -->